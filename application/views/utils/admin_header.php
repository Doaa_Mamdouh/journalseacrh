<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title><?php echo TITLE; ?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta
            content="Preview page of Metronic Admin Theme #3 for search results"
            name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->

        <link
            href="<?= base_url() ?>assets/global/plugins/font-awesome/css/font-awesome.min.css"
            rel="stylesheet" type="text/css" />
        <link
            href="<?= base_url() ?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css"
            rel="stylesheet" type="text/css" />
        <link
            href="<?= base_url() ?>assets/global/plugins/bootstrap/css/bootstrap.min.css"
            rel="stylesheet" type="text/css" />
        <link
            href="<?= base_url() ?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css"
            rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link
            href="<?= base_url() ?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"
            rel="stylesheet" type="text/css" />
        <link
            href="<?= base_url() ?>assets/global/plugins/fancybox/source/jquery.fancybox.css"
            rel="stylesheet" type="text/css" />
        <link
            href="<?= base_url() ?>assets/global/plugins/select2/css/select2.min.css"
            rel="stylesheet" type="text/css" />
        <link
            href="<?= base_url() ?>assets/global/plugins/select2/css/select2-bootstrap.min.css"
            rel="stylesheet" type="text/css" />
        <link
            href="<?= base_url() ?>assets/global/plugins/nouislider/nouislider.min.css"
            rel="stylesheet" type="text/css" />
        <link
            href="<?= base_url() ?>assets/global/plugins/nouislider/nouislider.pips.css"
            rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="<?= base_url() ?>assets/global/css/components.min.css"
              rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?= base_url() ?>assets/global/css/plugins.min.css"
              rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="<?= base_url() ?>assets/pages/css/search.min.css"
              rel="stylesheet" type="text/css" />
        <link href="<?= base_url() ?>assets/pages/css/advanced-search.min.css"
              rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="<?= base_url() ?>assets/layouts/layout3/css/layout.min.css"
              rel="stylesheet" type="text/css" />
        <link
            href="<?= base_url() ?>assets/layouts/layout3/css/themes/default.min.css"
            rel="stylesheet" type="text/css" id="style_color" />
        <link href="<?= base_url() ?>assets/layouts/layout3/css/custom.min.css"
              rel="stylesheet" type="text/css" />
        <link href="<?= base_url() ?>assets/layouts/layout3/css/custom.css"
              rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="<?= base_url() ?>images/shortcut-icon.png" />

        <link href="<?= base_url() ?>css/journals_custom_css.css" rel="stylesheet" type="text/css" />

        <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
        <script src="<?= base_url() ?>assets/js/jquery.plugin.min.js"></script>
        <script>
            $(document).ready(function () {
                var violated = false;
                function numberKeyDown(e,elem) {
                    var keynum;
                    if (window.event) { // IE                    
                        keynum = e.keyCode;
                    } else if (e.which) { // Netscape/Firefox/Opera                   
                        keynum = e.which;
                    }
                    //alert (keynum);
                    // Allow: backspace, delete, tab, escape, enter, up and down.
                    if ($.inArray(keynum, [46, 8, 9, 27, 13, 0, 39, 38, 40]) !== -1 ||
                    // Allow: Ctrl+A, Command+A
                    (keynum === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                    // Allow: Ctrl+C, Command+C
                    (keynum === 67 && (e.ctrlKey === true || e.metaKey === true)) ||
                    // Allow: Ctrl+X, Command+X
                    (keynum === 88 && (e.ctrlKey === true || e.metaKey === true)) ||
                    // Allow: Ctrl+V, Command+V
                    (keynum === 86 && (e.ctrlKey === true || e.metaKey === true)) ||
                    // Allow: Ctrl+Z, Command+Z
                    (keynum === 90 && (e.ctrlKey === true || e.metaKey === true)) ||
                    // Allow: home, end, left, right, down, up
                    (keynum >= 35 && keynum <= 37)) {
                    // let it happen, don't do anything
                    return;
                    } if (keynum == 110 || keynum == 190){ // dot
                        if(elem.indexOf(".") >= 0)
                            e.preventDefault();
                        else
                            return;
                    } if (((keynum < 48 || keynum > 57) && (keynum < 96 || keynum > 105)) || (keynum >= 38 && keynum <= 40)) {
                                                                e.preventDefault();
                                                            }
                                                        }
                                                        function textKeyDown(e, elem) {
                                                            var keynum;
                                                            if (window.event) { // IE                    
                                                                keynum = e.keyCode;
                                                            } else if (e.which) { // Netscape/Firefox/Opera                   
                                                                keynum = e.which;
                                                            }
//	alert (keynum);
                                                            // Allow: backspace, delete, tab, escape, enter, up and down.
                                                            if ($.inArray(keynum, [46, 8, 9, 27, 13, 110, 190, 39, 38, 40]) !== -1 ||
                                                                    // Allow: Ctrl+A, Command+A
                                                                            (keynum === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                                                                            // Allow: Ctrl+C, Command+C
                                                                                    (keynum === 67 && (e.ctrlKey === true || e.metaKey === true)) ||
                                                                                    // Allow: Ctrl+X, Command+X
                                                                                            (keynum === 88 && (e.ctrlKey === true || e.metaKey === true)) ||
                                                                                            // Allow: home, end, left, right, down, up
                                                                                                    (keynum >= 35 && keynum <= 37)) {
                                                                                        // let it happen, don't do anything
                                                                                        return;
                                                                                    }

                                                                                    if (((keynum >= 48 && keynum <= 57) || (keynum >= 96 && keynum <= 105)) || (keynum >= 38 && keynum <= 40)) {
                                                                                        e.preventDefault();
                                                                                    }
                                                                                }
                                                                                function textNumViolation() {
                                                                                    var violate = false;
                                                                                    $('*[id*=valid_span]:visible').each(function () {
                                                                                        //     alert($(this).text());

                                                                                        violate = true;
                                                                                    });
                                                                                    return violate;
                                                                                }
                                                                                function handlingTextNumberViolation(item) {
                                                                                    
                                                                                    var attr = item.attr('act');
                                                                                    var val = item.val();
                                                                                    //alert(val);
                                                                                    var matches = val.match(/\d+/g);

                                                                                     if ((item.attr("class").indexOf("date") < 0 && (matches != null && typeof attr == typeof undefined)) || (typeof attr !== typeof undefined && !$.isNumeric(val) && val != "")) {
                                                                                        $('#valid_span_' + item.attr('name')).css("display", "block");

                                                                                    } else /*if(!violated || (violated && val!="")||item.attr("type") == "text")*/ {
                                                                                        $('#valid_span_' + item.attr('name')).css("display", "none");
                                                                                        /*if(item.attr("type") == "number"){
                                                                                         violated  = false;
                                                                                         }*/
                                                                                    }
                                                                                }
                                                                                $(":input[type=text]").on('input', function () {
                                                                                    handlingTextNumberViolation($(this));
                                                                                });
                                                                                $(':input[type=text]').each(function () {
                                                                                    var attr = $(this).attr('act');
                                                                                    if ($(this).attr("class") && $(this).attr("class").indexOf(" date") >= 0)
                                                                                        $(this).after('<span id="valid_span_' + $(this).attr('name') + '" style="display:none;color:red;"><?php echo INVALID_DATE_VALUE_FORMAT; ?></span>');
                                                                                    else if (typeof attr == typeof undefined && $(this).attr("class") && $(this).attr("class").indexOf("no-validate") < 0)
                                                                                        $(this).after('<span id="valid_span_' + $(this).attr('name') + '" style="display:none;color:red;"><?php echo NO_NUMBERS; ?></span>');
                                                                                    else if (typeof attr !== typeof undefined) {
                                                                                        // $(this).attr("oninvalid", "setCustomValidity('<?php echo NO_TEXT; ?>')");
                                                                                        // $(this).attr("onchange", "setCustomValidity('')");
                                                                                        $(this).after('<span id="valid_span_' + $(this).attr('name') + '" style="display:none;color:red;"><?php echo NO_TEXT; ?></span>');
                                                                                    }
                                                                                    /* $(this).bind("paste",function(e) {
                                                                                     //  var pastedData = e.originalEvent.clipboardData.getData('text');
                                                                                     handlingTextNumberViolation($(this));
                                                                                     
                                                                                     });*/
                                                                                });
                                                                                $(':input[type=text]').on('keydown', function (e) {
                                                                                    var attr = $(this).attr('act');

                                                                                    if (typeof attr == typeof undefined && $(this).attr("class").indexOf("no-validate") < 0 && $(this).attr("class").indexOf("date") < 0)
                                                                                        textKeyDown(e, $(this).val());
                                                                                    else if (typeof attr !== typeof undefined) {
                                                                                        numberKeyDown(e, $(this).val());



                                                                                    }

                                                                                });
                                                                            });
        </script>
    </head>
    <!-- END HEAD -->

    <body>

        <div class="page-wrapper-row">
            <div class="page-wrapper-top">
                <!-- BEGIN HEADER -->
                <div class="page-header">
                    <!-- BEGIN HEADER TOP -->
                    <div class="page-header-top">
                        <div class="container">
                            <!-- BEGIN LOGO -->
                            <div class="page-logo">
                                <a href="<?php echo site_url('admin') ?>"> <img
                                        src="<?= base_url() ?>assets/layouts/layout3/img/logo-default.jpg"
                                        alt="logo" class="logo-default">
                                </a>
                            </div>
                            <!-- END LOGO -->
                            <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                            <a href="javascript:;" class="menu-toggler"></a>
                            <!-- END RESPONSIVE MENU TOGGLER -->
                            <!-- BEGIN TOP NAVIGATION MENU -->
                            <div class="top-menu">
                                <ul class="nav navbar-nav pull-right">
                                    <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                                    <?php if ($this->session->userdata('adminid')) { ?>
                                        <li class="dropdown-dark">
                                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                                <span class="username username-hide-mobile font-dark"><?php echo $login_user_name ?></span>
                                            </a>
                                        </li>
                                        <li class="login-out-common" ><a class="bold btn red" href="<?= site_url('admin/logoff') ?>"> 
                                        <i
                                                    class="icon-logout"></i>
                                                    <?php echo LOGOUT; ?>
                                            </a>
                                        </li>
                                    <?php } else { ?>
                                        <li  class="login-out-common" ><a class="bold btn blue" href="<?= site_url('admin/login') ?>" > <i
                                                    class="icon-login"></i>
                                                <?php echo LOGIN; ?>
                                            </a>
                                        </li>
                                    <?php } ?>
                                    <!-- END QUICK SIDEBAR TOGGLER -->
                                </ul>
                            </div>
                            <!-- END TOP NAVIGATION MENU -->
                        </div>
                    </div>
                    <!-- END HEADER TOP -->

                </div>
                <!-- END HEADER -->
            </div>
        </div>

        <!--[if lt IE 9]>   
        <script src="<?= base_url() ?>assets/global/plugins/respond.min.js"></script>
        <script src="<?= base_url() ?>assets/global/plugins/excanvas.min.js"></script> 
        <script src="<?= base_url() ?>assets/global/plugins/ie8.fix.min.js"></script> 
        <![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="<?= base_url() ?>assets/global/plugins/jquery.min.js"
        type="text/javascript"></script>
        <script
            src="<?= base_url() ?>assets/global/plugins/bootstrap/js/bootstrap.min.js"
        type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/global/plugins/js.cookie.min.js"
        type="text/javascript"></script>
        <script
            src="<?= base_url() ?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"
        type="text/javascript"></script>
        <script
            src="<?= base_url() ?>assets/global/plugins/jquery.blockui.min.js"
        type="text/javascript"></script>
        <script
            src="<?= base_url() ?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"
        type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script
            src="<?= base_url() ?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"
        type="text/javascript"></script>
        <script
            src="<?= base_url() ?>assets/global/plugins/fancybox/source/jquery.fancybox.pack.js"
        type="text/javascript"></script>
        <script
            src="<?= base_url() ?>assets/global/plugins/nouislider/wNumb.min.js"
        type="text/javascript"></script>
        <script
            src="<?= base_url() ?>assets/global/plugins/nouislider/nouislider.min.js"
        type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<?= base_url() ?>assets/global/scripts/app.min.js"
        type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?= base_url() ?>assets/pages/scripts/search.min.js"
        type="text/javascript"></script>
        <script
            src="<?= base_url() ?>assets/global/plugins/select2/js/select2.full.min.js"
        type="text/javascript"></script>
        <script
            src="<?= base_url() ?>assets/pages/scripts/components-select2.min.js"
        type="text/javascript"></script>
        <script
            src="<?= base_url() ?>assets/pages/scripts/components-nouisliders-advanced-search.min.js"
        type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script
            src="<?= base_url() ?>assets/layouts/layout3/scripts/layout.min.js"
        type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/layouts/layout3/scripts/demo.min.js"
        type="text/javascript"></script>
        <script
            src="<?= base_url() ?>assets/layouts/global/scripts/quick-sidebar.min.js"
        type="text/javascript"></script>
        <script
            src="<?= base_url() ?>assets/layouts/global/scripts/quick-nav.min.js"
        type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
    </body>

</html>
