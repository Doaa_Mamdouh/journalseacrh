<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<?php
// session_start ();
$this->load->view ( 'localization/lang' );
$this->load->view ( 'localization/txt' );
defineLocale ();
defineStrings ();
?>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" dir="rtl">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
<script type="text/javascript">

function validateEmail(email) {
	//var re = /\S+@\S+\.\S+/;
	var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(email);
}

function checkValidBasicInfo(email_valdation){
	var username= null;
	if(document.forms["basic_info_form"]["username"] != null){
		username = document.forms["basic_info_form"]["username"].value;
	}
	var first_name =null;
	if(document.forms["basic_info_form"]["first_name"] != null){
		first_name = document.forms["basic_info_form"]["first_name"].value;
	}
	var last_name =null;
	if(document.forms["basic_info_form"]["last_name"] != null){
		last_name = document.forms["basic_info_form"]["last_name"].value;
	}
	var mobile_number = document.forms["basic_info_form"]["mobile_number"].value;
	var email = document.forms["basic_info_form"]["email"].value;
	
	if (username == "") {
		$("#username_tooltip").css("display","block");
		$("#username_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#username_tooltip").css("display","none");
		$("#username_tooltip").css("visibility","none");
	}
	if (first_name == "") {
		$("#first_name_tooltip").css("display","block");
		$("#first_name_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#first_name_tooltip").css("display","none");
		$("#first_name_tooltip").css("visibility","none");
	}
	if (last_name == "") {
		$("#last_name_tooltip").css("display","block");
		$("#last_name_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#last_name_tooltip").css("display","none");
		$("#last_name_tooltip").css("visibility","none");
	}
	var mobile_pattern = /05/g;
	if (mobile_number == "" || (mobile_number.length < 10 || mobile_number.length > 10 ||
        !mobile_pattern.test(mobile_number))) {
		$("#mobile_number_tooltip").css("display","block");
		$("#mobile_number_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#mobile_number_tooltip").css("display","none");
		$("#mobile_number_tooltip").css("visibility","none");
	}
	if (email == "") {
		$("#email_tooltip").css("display","block");
		$("#email_tooltip").css("visibility","visible");
		return false;
	} else if (!validateEmail(email)) {
		$("#email_tooltip").text(email_valdation);
		$("#email_tooltip").css("display","block");
		$("#email_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#email_tooltip").css("display","none");
		$("#email_tooltip").css("visibility","none");
	}
}

function changeAvatar() {
	document.getElementById('avatarForm').submit();
}

function cancelAvatarChange(img_src) {
	$("#img_avatar").attr("src", img_src);

}

function changePassword() {
	document.getElementById('passwordForm').submit();
}

function checkPassword(){

var old_password= document.forms["passwordForm"]["old_password"].value;
	var new_password= document.forms["passwordForm"]["new_password"].value;
	var retyped_new_password= document.forms["passwordForm"]["retyped_new_password"].value;
	
	if (old_password== "") {
		$("#old_password_tooltip").css("display","block");
		$("#old_password_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#old_password_tooltip").css("display","none");
		$("#old_password_tooltip").css("visibility","none");
	}
	if (new_password== "") {
		$("#new_password_tooltip").css("display","block");
		$("#new_password_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#new_password_tooltip").css("display","none");
		$("#new_password_tooltip").css("visibility","none");
	}
	if (retyped_new_password== "") {
		$("#retyped_new_password_tooltip").css("display","block");
		$("#retyped_new_password_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#retyped_new_password_tooltip").css("display","none");
		$("#retyped_new_password_tooltip").css("visibility","none");
	}
	
	document.getElementById('incorrect_old_password').style.display = 'none';
	document.getElementById('incorrect_repeated_password').style.display = 'none';
	var saved_password = document.getElementById('saved_password').value;
	var old_password = document.getElementById('old_password').value;
	var new_password = document.getElementById('new_password').value;
	var retyped_new_password = document.getElementById('retyped_new_password').value;
	if(saved_password != old_password){
		document.getElementById('incorrect_old_password').style.display = 'block';
		return false;
	} else if (new_password != retyped_new_password) {
		document.getElementById('incorrect_repeated_password').style.display = 'block';
		return false;
	}
	return true;
}

function checkValid() {
	var department = document.forms["additional_info_form"]["department"].value;
	var job = document.forms["additional_info_form"]["job"].value;
	
	if (department == "") {
		$("#department_tooltip").css("display","block");
		$("#department_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#department_tooltip").css("display","none");
		$("#department_tooltip").css("visibility","none");
	}
	if (job == "") {
		$("#job_tooltip").css("display","block");
		$("#job_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#job_tooltip").css("display","none");
		$("#job_tooltip").css("visibility","none");
	}
}
</script>

<style type="text/css">
	.tabbable-line > .nav-tabs > li.active {
		box-shadow: 0px -4px 0px #3c97c1 inset;
		border-bottom: none !important;
	}
    .tabbable-line > .nav-tabs > li.open, .tabbable-line > .nav-tabs > li:hover {
      	background: none;
		box-shadow: 0px -4px 0px #3c97c1 inset;
		border-bottom: none !important;
     }
</style>

</head>

<body>
	<div class="page-wrapper">
	<?php $this->load->view ( 'users/user_header_and_menu' ); ?>
	<?php $this->load->view ('utils/collap_table_script');?>

		<div id="tableDiv" style="display: none;">
			<!-- BEGIN CONTAINER -->
			<div class="page-container">
				<div class="row">
					<div class="col-md-12">
						<!-- BEGIN PROFILE SIDEBAR -->
						<div class="profile-sidebar">
							<!-- PORTLET MAIN -->
							<div class="portlet light profile-sidebar-portlet ">
								<!-- SIDEBAR USERPIC -->
								<div class="profile-userpic">
									<?php if(isset($profile_img) && $profile_img != '') {?>
									<img src="<?php echo base_url()?><?php echo $profile_img?>"
										 class="img-responsive" alt="">
									<?php } else {?>	
									<img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image"
										class="img-responsive" alt="">
									<?php }?>
                                </div>
								<!-- END SIDEBAR USERPIC -->
								<!-- SIDEBAR USER TITLE -->
								<div class="profile-usertitle">
									<div class="profile-usertitle-name"> <?php echo $first_name.' '.$last_name ; ?> </div>
									<div class="profile-usertitle-job">  <?php
										if ($type == "user") {
											echo USER;
										} else if ($type== "manager") {
											echo MANAGER;
										}
									?> </div>
								</div>
								<!-- END SIDEBAR USER TITLE -->
							</div>
							<!-- END PORTLET MAIN -->
							<!-- END SIDEBAR USER TITLE -->
						</div>
						<!-- END PORTLET MAIN -->
						<!-- END BEGIN PROFILE SIDEBAR -->
						<div class="profile-content">

							<!-- BEGIN CONTENT -->
							<div class="portlet light">
								<div class="portlet-title tabbable-line">
									<div class="caption caption-md">
										<i class="icon-globe theme-font hide"></i> <span
											class="caption-subject font-blue-madison bold uppercase"><?php echo $first_name.' '.$last_name ; ?></span>
									</div>
									<ul class="nav nav-tabs">
										<li class="active"><a href="#tab_1_1" data-toggle="tab"> <?php echo BASIC_INFO; ?> </a></li>
										<li><a href="#tab_1_2" data-toggle="tab"> <?php echo CHANGE_PROFILE_PIC; ?> </a></li>
										<li><a href="#tab_1_3" data-toggle="tab"> <?php echo CHANGE_PASSWORD; ?> </a></li>
										<li><a href="#tab_1_4" data-toggle="tab"> <?php echo MORE_INFO;?> </a></li>
									</ul>
								</div>
								<div class="portlet-body form">
									<div class="tab-content">
										<!-- PERSONAL INFO TAB -->
										<div class="tab-pane active" id="tab_1_1">
											<form id="basic_info_form" class="form-horizontal"
												role="form" method='post'
												action="<?php echo site_url('user_profile/view_edit_general_profile/'.$id)?>"
												onsubmit="javascript:return checkValidBasicInfo('<?php echo INVALID_EMAIL; ?>');">
												<div class="form-body">
													<div class="form-group">
														<label class="col-md-3 control-label"><?php echo USERNAME; ?><span
															class="required"> * </span></label>
														<div class="col-md-6">
															<input type="text" class="form-control " name="username"
																id="username" value="<?php echo  $username; ?>"
																onfocus="javascript:removeTooltip('username_tooltip');">
															<span id="username_tooltip" class="tooltiptext"
																style="display: none; color: red;"><?php echo FILL_THIS_FIELD; ?></span>
														</div>
													</div>
													<div class="form-group">
														<label class="col-md-3 control-label"><?php echo FIRST_NAME; ?> <span
															class="required"> * </span></label>
														<div class="col-md-6">
															<input type="text" class="form-control "
																name="first_name" value="<?php echo $first_name; ?>"
																id="first_name"
																onfocus="javascript:removeTooltip('first_name_tooltip');">
															<span id="first_name_tooltip" class="tooltiptext"
																style="display: none; color: red;"><?php echo FILL_THIS_FIELD; ?></span>
														</div>
													</div>
													<div class="form-group">
														<label class="col-md-3 control-label"><?php echo LAST_NAME; ?> <span
															class="required"> * </span></label>
														<div class="col-md-6">
															<input type="text" class="form-control " name="last_name"
																value="<?php echo  $last_name; ?>" id="last_name"
																onfocus="javascript:removeTooltip('last_name_tooltip');">
															<span id="last_name_tooltip" class="tooltiptext"
																style="display: none; color: red;"><?php echo FILL_THIS_FIELD; ?></span>
														</div>
													</div>
													<div class="form-group">
														<label class="col-md-3 control-label"><?php echo MOBILE_NUMBER; ?> <span
															class="required"> * </span></label>
														<div class="col-md-6">
															<input type="number" class="form-control no-spinners"
																onkeydown="numberKeyDown(event)" name="mobile_number"
																id="mobile_number" value="<?php echo $mobile_number?>"
																onfocus="javascript:removeTooltip('mobile_number_tooltip');">
															<span id="mobile_number_tooltip" class="tooltiptext"
																style="display: none; color: red"><?php echo MOBILE_NUMBER_INVALID; ?></span>

														</div>
													</div>
													<div class="form-group">
														<label class="col-md-3 control-label"><?php echo EMAIL; ?> <span
															class="required"> * </span></label>
														<div class="col-md-6">
															<input type="text" class="form-control " name="email"
																value="<?php echo $email?>" id="email"
																onfocus="javascript:removeTooltip('email_tooltip');"> 
															  <span
																id="email_tooltip" class="tooltiptext"
																<?php if (isset($email_exists_error) && $email_exists_error == 1){?>
																style="display: block; color: red;" <?php }else {?>
																style="display: none; color:red;" <?php }?>><?php
																if (isset ( $email_exists_error ) && $email_exists_error == 1) {
																	echo EMAIL_IS_EXIST;
																} else {
																	echo INVALID_EMAIL;
																}
																?>
															 </span>
														</div>
													</div>
													<div class="margin-top-10">
														<button type="submit" class="btn blue"><?php echo SAVE ?></button>
													</div>
												</div>
											</form>
										</div>
										<!-- END PERSONAL INFO TAB -->
										<!-- CHANGE AVATAR TAB -->
										<div class="tab-pane" id="tab_1_2">
											<p> <?php echo CHANGE_AVATAR_DESCRIPTION; ?>  </p>
											<form id="avatarForm" method="post"
												action="<?php echo site_url('user_profile/change_avatar/'.$id)?>"
												role="form" method='post' enctype="multipart/form-data">
												<div class="form-group">
													<div class="fileinput fileinput-new"
														data-provides="fileinput">
														<div class="fileinput-new thumbnail"
															style="width: 200px; height: 150px;">
															<?php if(isset($profile_img) && $profile_img != ''){?>
																<img id="img_avatar" src="<?php echo base_url()?><?php echo $profile_img?>" alt="" />
															<?php }else{ ?>
																<img id="img_avatar" src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
															<?php } ?>
														</div>
														<div class="fileinput-preview fileinput-exists thumbnail"
															style="max-width: 200px; max-height: 150px;"></div>
														<div>
															<span class="btn default btn-file"> 
															<span
																class="fileinput-new"> <?php echo SELECT_IMAGE ; ?>
															</span>
																<span class="fileinput-exists"> <?php echo CHANGE_PROFILE_PIC; ?></span>
																<input type="file" name="avatar" accept="image/*">
															</span> <a href="javascript:;"
																class="btn default fileinput-exists"
																data-dismiss="fileinput"> <?php echo DELETE; ?></a>
														</div>
													</div>
												</div>
												<div class="margin-top-10">
													<a href="javascript:changeAvatar();" class="btn blue"> <?php echo SAVE; ?></a>
													<a href="javascript:cancelAvatarChange('<?php echo base_url()?><?php echo $profile_img?>');"
														class="btn default"> <?php echo CANCEL; ?></a>
												</div>

											</form>
										</div>
										<!-- END CHANGE AVATAR TAB -->
										<!-- CHANGE PASSWORD TAB -->
										<div class="tab-pane" id="tab_1_3">
											<div class="form-body">
												<div class="form-group" id="incorrect_old_password"
													style="display: none; color: red;">
													<?php echo INCORRECT_OLD_PASSWORD;?>
												</div>
												<div class="form-group" id="incorrect_repeated_password"
													style="display: none; color: red;">
													<?php echo INCORRECT_REPEATED_PASSWORD;?>
												</div>
												<form id="passwordForm" class="form-horizontal"
													action="<?php echo site_url('user_profile/change_password/'.$id)?>"
													role="form" method='post'
													onsubmit="javascript:return checkPassword();">
													<div class="form-group">
														<label class="col-md-3 control-label"><?php echo CURRENT_PASSWORD ; ?> <span
															class="required"> * </span> </label>
														<div class="col-md-6">
															<input type="password" name="old_password"
																id="old_password" class="form-control "
																onfocus="javascript:removeTooltip('old_password_tooltip');">
															<span id="old_password_tooltip" class="tooltiptext"
																style="display: none; color: red;"><?php echo FILL_THIS_FIELD; ?><span
																class="required"> * </span></span> <input type="hidden"
																value="<?php echo $password?>" id="saved_password" />
														</div>
													</div>
													<div class="form-group">
														<label class="col-md-3 control-label"><?php echo NEW_PASSWORD ; ?><span
															class="required"> * </span></label>
														<div class="col-md-6">
															<input type="password" name="new_password"
																id="new_password" class="form-control "
																onfocus="javascript:removeTooltip('new_password_tooltip');">
															<span id="new_password_tooltip" class="tooltiptext"
																style="display: none; color: red;"><?php echo FILL_THIS_FIELD; ?></span>
														</div>
													</div>
													<div class="form-group">
														<label class="col-md-3 control-label"><?php echo REPEAT_NEW_PASSWORD ; ?><span
															class="required"> * </span></label>
														<div class="col-md-6">
															<input type="password" name="retyped_new_password"
																id="retyped_new_password" class="col-md-6 form-control "
																onfocus="javascript:removeTooltip('retyped_new_password_tooltip');">
															<span id="retyped_new_password_tooltip"
																class="tooltiptext" style="display: none; color: red;"><?php echo FILL_THIS_FIELD; ?></span>
														</div>
													</div>
													<div class="margin-top-10">
														<!-- <a href="javascript:changePassword();" class="btn green"> <?php echo CHANGE_PASSWORD ; ?> </a>-->
														<button type="submit" class="btn blue"><?php echo CHANGE_PASSWORD ; ?></button>
													</div>
												</form>
											</div>
										</div>
										<!-- END CHANGE PASSWORD TAB -->
										<!-- ADDITIONAL INFO TAB -->
										<div class="tab-pane" id="tab_1_4">
											<div class="form-body">
												<form id="additional_info_form" role="form" method="post"
													onsubmit="javascript:return checkValid('<?php echo INVALID_EMAIL; ?>');"
													action="<?php echo site_url('user_profile/change_additional_info/'.$id)?>"
													class="form-horizontal">
													<div class="form-body">
														<div class="form-group">
															<label class="col-md-3 control-label"><?php echo USER_ORGANIZATION_NAME; ?> <span
																class="required"> * </span></label>
															<div class="col-md-6">
																<select class="form-control " name="organization"
																	id="organization" class="form-control" disabled>
																  	<?php
																		if ($available_organizations) {
																			$organization_id = $available_organizations ["id"];
																			$organization_name = $available_organizations ["organization_name"];
																	?>
																		<option value="<?php echo $organization_id?>"
																						<?php if ($organization == $organization_name){?>
																						selected="selected" <?php }?>> <?php echo $organization_name?>
																		</option>
																	<?php
																		} else {
																	?>
																	<option value="" selected></option>
																	<?php
																		}
																	?>		
																</select>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-3 control-label"><?php echo USER_DEPARTMENT; ?> <span
																class="required"> * </span></label>
															<div class="col-md-6">
																<input type="text" class="form-control "
																	name="department" value="<?php echo  $department; ?>"
																	id="department"
																	onfocus="javascript:removeTooltip('department_tooltip');">
																<span id="department_tooltip" class="tooltiptext"
																	style="display: none; color: red;"><?php echo FILL_THIS_FIELD; ?></span>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-3 control-label"><?php echo USER_JOB; ?> <span
																class="required"> * </span></label>
															<div class="col-md-6">
																<input type="text" class="form-control " name="job"
																	value="<?php echo  $job; ?>" id="job"
																	onfocus="javascript:removeTooltip('job_tooltip');"> <span
																	id="job_tooltip" class="tooltiptext"
																	style="display: none; color: red;"><?php echo FILL_THIS_FIELD; ?></span>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-3 control-label"><?php echo MANAGER; ?> <span
																class="required"> * </span></label>
															<div class="col-md-6">
															<?php
															if ($available_users) {
																?>
																<select class="form-control " name="manager" id="manager"
																					class="form-control">
																					<option value="" selected="selected">---------</option>
																  	<?php
																for($i = 0; $i < count ( $available_users ); $i ++) {
																	$manager_id = $available_users [$i] ["id"];
																	$manager_name = $available_users [$i] ["first_name"];
																	?>
																		<option value="<?php echo $manager_id?>"
																						<?php if ($manager == $manager_id){?>
																						selected="selected" <?php }?>> <?php echo $manager_name?>
																		</option>
																	<?php
																}
																?>
																</select> 
																<?php
																} else {
																?>
																	<select class="form-control " name="manager" id="manager"
																						class="form-control" disabled>
																						<option value="" selected="selected"></option>
																	</select>
																<?php
																}
																?>		
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-3 control-label"><?php echo DATE_TYPE; ?> <span
																class="required"> * </span></label>
															<div class="col-md-6">
																<input type="radio" name="date_type" value="hijri" <?php if($date_type == "hijri" ){ ?> checked <?php } ?>  > <?php echo HIJRI; ?><br>
																<input type="radio" name="date_type" value="gregorian" <?php if($date_type == "gregorian" ){ ?> checked <?php } ?> > <?php echo GREGORIAN; ?><br>
															</div>
														</div>
														<div class="margin-top-10">
															<button type="submit" class="btn blue"><?php echo SAVE ; ?></button>
														</div>
													</div>
												</form>
											</div>
										</div>
										<!-- END ADDITIONAL INFO TAB -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php $this->load->view('utils/footer.php');?>
	<script>
		fillDiv("tableDiv");
		changeBackGroundColor();
	</script>
</body>
</html>