    <script src="<?php echo base_url()?>assets/js/jquery.aCollapTable.js"></script>
    <script>
        $(document).ready(function(){
            $('#permissions').aCollapTable({
                startCollapsed: true,
                addColumn: false,
                plusButton: '<i class="glyphicon glyphicon-plus"></i> ', 
                minusButton: '<i class="glyphicon glyphicon-minus"></i> ' 
            });
        });
    </script>