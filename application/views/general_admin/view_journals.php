<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<?php
session_start();
$this->load->view('localization/lang');
$this->load->view('localization/txt');
defineLocale();
defineStrings();
?>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title><?php echo TITLE; ?></title>
        <link href="<?= base_url() ?>css/journals_custom_css.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url() ?>assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />

        <script
        src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script>
            var journals = null;
            var page_size = 8;
            var allRecordsMap = new Array();
            allRecordsMap["1"] = journals;
            var total_count = 0;
            var search_text = "";
            var clickedPage = 1;
<?php if ($this->session->flashdata('action') != null) { ?>
                search_text = sessionStorage.getItem("search_text");
                clickedPage = sessionStorage.getItem("clickedPage");
                total_count = sessionStorage.getItem("total_count");
<?php } else { ?>
                sessionStorage.setItem("search_text", "");
<?php } ?>

            window.onload = function () {
                uploadRecordsPerPage(clickedPage);
                $('#quick_search_input').val(search_text);
            };
            function uploadRecordsPerPage(clickedPage) {
                sessionStorage.setItem("clickedPage", clickedPage + "");
                if (allRecordsMap[clickedPage] == null) {
                    var url = '<?php echo site_url("admin_panel/getJournalsList") ?>' + '/' + page_size + '/' + clickedPage;
                    if (search_text != null && search_text != "") {
                        url = '<?php echo site_url("search_journals/executeBasicSearch") ?>' + '/' + encodeURIComponent(search_text) + '/' + page_size + '/' + clickedPage;
                    }
                    $.ajax({
                        url: url,
                        success: function (response) {
                            var responseObject = JSON.parse(response);
                            //alert(responseObject["journals"]);
                            allRecordsMap[clickedPage] = responseObject["journals"];
                            sessionStorage.setItem("total_count", responseObject["total_count"]);
                            adjustPage(responseObject["total_count"], clickedPage);

                        },
                        error: function (request, status, error) {
                            alert(request.responseText);
                        }
                    });

                } else {
                    getNextBatch(clickedPage);
                    adjustPage(total_count, clickedPage);
                }
            }
            function adjustPage(count, clickedPage) {
                total_count = count;
                if (total_count <= 8) {
                    $("#pagination_div").css('display', 'none');
                } else {
                    var page_num = Math.ceil(total_count / page_size);
                    $("#pagination_div").css('display', 'block');
                    $("#total_pages_num").text(page_num);
                    $("#desired_page_num").val(clickedPage);
                    if (clickedPage == 1) {
                        $("#next_button").prop('disabled', false);
                        $("#prev_button").prop('disabled', true);
                    } else if (clickedPage == page_num) {
                        $("#next_button").prop('disabled', true);
                        $("#prev_button").prop('disabled', false);
                    } else {
                        $("#next_button").prop('disabled', false);
                        $("#prev_button").prop('disabled', false);
                    }
                }
                getNextBatch(clickedPage);

            }
            function getNextBatch(clickedPage) {
                journals = allRecordsMap[clickedPage];
                $('#journals').empty();
                for (var i in journals) {
                    if (journals[i]["financial_times_ranking"] == 1)
                        journals[i]["financial_times_ranking"] = "Yes";
                    else
                        journals[i]["financial_times_ranking"] = "No";
                    var updateUrl = "<?= site_url('admin_panel/updateJournal/') ?>" + journals[i]["id"];
                    var deleteUrl = "javascript:delBtnClick('" + journals[i]["id"] + "', 'journals');";
                    if (journals[i] ["domicile_name"] == null) {
                        journals[i] ["domicile_name"] = "";
                    }
                    $('#journals').append('<tr>' +
                            '<td>' + journals[i]["title"] + '</td>' +
                            '<td>' + journals[i]["field"] + '</td>' +
                            '<td>' + journals[i]["abs_rank"] + '</td>' +
                            '<td>' + journals[i]["domicile_name"] + '</td>' +
                            '<td>' + journals[i]["impact_factor"] + '</td>' +
                            '<td>' + journals[i]["articles_num"] + '</td>' +
                            '<td>' + journals[i]["financial_times_ranking"] + '</td>' +
                            '<td><table><tbody id=buttons><tr><td><a href="' + updateUrl + '" class="btn btn-outline btn-circle btn-sm blue"> <i class="fa fa-edit"></i> <?php echo EDIT; ?> </a></td> <td> <a id="delBtn" href="' + deleteUrl + '"class="btn btn-outline btn-circle dark btn-sm red"> <i class="fa fa-trash-o"></i> <?php echo DELETE; ?> </a> </td> </tr> </tbody></table></td> </tr>');
                }
                $('html, body').animate({
                    scrollTop: $("#all-page").offset().top
                }, 500);
                $('#loading').addClass('hidden');
            }


            function get_prev_page() {
                var current_page = parseInt($("#desired_page_num").val());
                var newpage = (current_page - 1);
                paginate_on_page(newpage);
            }

            function get_next_page() {
                var current_page = parseInt($("#desired_page_num").val());
                var newpage = (current_page + 1);
                paginate_on_page(newpage);
            }

            function paginate_on_page(newpage) {
                $('#loading').removeClass('hidden');
                var no_of_pages = Math.ceil(total_count / page_size);
                if (parseInt(newpage) >= parseInt(no_of_pages)) {
                    newpage = no_of_pages;
                    $("#next_button").prop('disabled', true);
                } else {
                    $("#next_button").prop('disabled', false);
                }
                if (parseInt(newpage) > 1) {
                    $("#prev_button").prop('disabled', false);
                } else {
                    $("#prev_button").prop('disabled', true);
                }
                $("#calculated_current_page").val(newpage);
                $("#desired_page_num").val(newpage);
                uploadRecordsPerPage(newpage);
            }
            function apply_quick_search() {
                $('#loading').removeClass('hidden');
                //document.getElementById('quickSearchForm').submit();
                search_text = $('#quick_search_input').val();
                sessionStorage.setItem("search_text", search_text);
                allRecordsMap = new Array();
                uploadRecordsPerPage(1);
            }
            $(document).on('keyup', '#desired_page_num', function (event) {
                if (event.keyCode == 13) {

                    var newpage = $('#desired_page_num').val();
                    paginate_on_page(newpage);
                }
            });
            $(document).on('keyup keypress', '#quickSearchForm', function (e) {
                var keyCode = e.keyCode || e.which;
                if (keyCode === 13) {
                    $('#loading').removeClass('hidden');
                    //document.getElementById('quickSearchForm').submit();
                    search_text = $('#quick_search_input').val();
                    sessionStorage.setItem("search_text", search_text);
                    allRecordsMap = new Array();
                    uploadRecordsPerPage(1);
                    e.preventDefault();
                    return false;
                }
            });
        </script>
    </head>
    <!-- END HEAD -->
    <body class="page-container-bg-solid">
        <div class="page-wrapper">
            <?php include 'general_admin_header.php'; ?>
            <div class="page-wrapper-row full-height">

                <!-- BEGIN CONTAINER -->
                <div class="page-container">
                    <!-- BEGIN CONTENT -->
                    <div class="page-content-wrapper">
                        <!-- BEGIN PAGE HEAD-->
                        <div class="page-head">
                            <div class="container">
                                <!-- BEGIN PAGE TITLE -->
                                <div class="page-title">
                                    <h1><?php echo JOURNALS; ?></h1>
                                    <a class="btn dark btn-outline btn-circle btn-sm" style="margin-top: 10px"
                                       href="<?= site_url('admin_panel/createJournal/') ?>"
                                       data-hover="dropdown" data-close-others="true"> <?php echo INSERT; ?>
                                    </a>
                                </div>
                                <!-- END PAGE TITLE -->
                                <?php $this->load->view('utils/quick_search'); ?>
                            </div>
                        </div>
                        <!-- END PAGE HEAD-->
                        <!-- BEGIN CONTENT BODY -->
                        <div class="container"> 
                            <?php $this->load->view('utils/delete_alert'); ?>
                            <!-- BEGIN BORDERED TABLE PORTLET-->
                            <div id="all-page" class="row">
                                <div class="col-md-12">
                                    <div class="portlet-body">
                                        <div class="table-scrollable">
                                            <table id="table-pagination" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th class="table-header" style="width: 270px;"><?php echo JOURNAL_TITLE; ?></th>
                                                        <th class="table-header" style="width: 200px;"><?php echo JOURNAL_FIELD; ?></th>
                                                        <th class="table-header" style="width: 55px;"><?php echo JOURNAL_ABSRANK; ?></th>
                                                        <th class="table-header" style="width: 150px;"><?php echo JOURNAL_DOMICILE; ?></th>
                                                        <th class="table-header" style="width: 70px;"><?php echo JOURNAL_IMPACT_FACTOR; ?></th>
                                                        <th class="table-header" style="width: 143px;"><?php echo JOURNAL_FREQUENCY; ?></th>
                                                        <th class="table-header" style="width: 122px;"><?php echo FINANCIAL_TIMES_RANKING; ?></th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody id="journals">

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <!-- END BORDERED TABLE PORTLET-->
                                    <input type="hidden" name="calculated_current_page" id="calculated_current_page" value="1">
                                    <input type="hidden" name="no_of_pages" id="no_of_pages" value="<?php echo $total_count; ?>">

                                    <div id="pagination_div" class="search-pagination pagination-rounded"
                                         style="margin: auto; text-align: center;display:
                                         <?php if (ceil($total_count / 8) <= 0) { ?>
                                             none
                                         <?php } else { ?>
                                             block
                                         <?php } ?>
                                         ">
                                        <div id = "pagination_form" >
                                            <input type="hidden" name="current_page" id="current_page" value="1">
                                            <input type="button" class="bold btn dark" style="width: 9rem !important;"
                                                   id="prev_button" disabled
                                                   onClick="javascript:get_prev_page()" value="<?php echo PREVIOUS; ?>" />
                                            <label><?php echo PAGE_NUM; ?>:</label>
                                            <input id="desired_page_num" type="number" class="pagination-input" value="1" min="1" />/ <span id="total_pages_num"><?= ceil($total_count / 8) ?></span>
                                            <input type="button" class="bold btn dark" style="width: 9rem !important;"
                                                   id="next_button"
                                                   onClick="javascript:get_next_page()" value="<?php echo NEXT; ?>" />
                                        </div>
                                    </div>
                                    <br>
                                </div>
                            </div>		
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <?php $this->load->view('utils/footer'); ?>
        <script>
            $(document).ready(function () {
                $('#loading').addClass('hidden');
            });
        </script>
    </body>

</html>