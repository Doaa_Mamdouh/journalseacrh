<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<?php
session_start();
$this->load->view('localization/lang');
$this->load->view('localization/txt');
defineLocale();
defineStrings();
?>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <script type="text/javascript">
            function checkValid(email_valdation) {
                var title = document.forms["myForm"]["title"].value;
                var field = document.forms["myForm"]["field"].value;
                if (title == "") {
                    $("#title_tooltip").css("display", "block");
                    $("#title_tooltip").css("visibility", "visible");
                    return false;
                } else {
                    $("#title_tooltip").css("display", "none");
                    $("#title_tooltip").css("visibility", "none");
                }
                if (field == "") {
                    $("#field_tooltip").css("display", "block");
                    $("#field_tooltip").css("visibility", "visible");
                    return false;
                } else {
                    $("#field_tooltip").css("display", "none");
                    $("#field_tooltip").css("visibility", "none");
                }
                return true;
            }
        </script>
        <meta charset="utf-8" />
        <title><?php echo TITLE; ?></title>
        <link href="<?= base_url() ?>css/journals_custom_css.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url() ?>assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
    </head>
    <!-- END HEAD -->
    <body class="page-container-bg-solid">
        <div class="page-wrapper">
            <?php include 'general_admin_header.php'; ?>
            <br>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <!-- BEGIN CONTAINER -->
                    <div class="page-container">
                        <!-- BEGIN CONTENT -->
                        <div class="page-content-wrapper">
                            <!-- BEGIN CONTENT BODY -->
                            <!-- BEGIN PAGE HEAD-->
                            <div class="page-head">
                                <div class="container">
                                    <div class="col-md-12">
                                        <div class="portlet box grey-mint">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-gift"></i>
                                                    <?php
                                                    if ($id) {
                                                        echo $title;
                                                    } else {
                                                        echo ADD_RECORD;
                                                    }
                                                    ?>
                                                </div>

                                            </div>
                                            <div class="portlet-body form">
                                                <!-- BEGIN FORM-->
                                                <form id="myForm" autocomplete="off" role="form"
                                                      method="post"
                                                      onsubmit="javascript:return checkValid('<?php echo INVALID_EMAIL; ?>');"
                                                      action="<?php
                                                      if ($id) {
                                                          echo site_url('admin_panel/updateJournal/' . $id);
                                                      } else {
                                                          echo site_url('admin_panel/createJournal/');
                                                      }
                                                      ?>"
                                                      class="form-horizontal">
                                                    <div class="form-body">
                                                        <div class="row">
                                                            <div class="col-md-12">

                                                                <div class="form-group" <?php if (isset($code) && $code == "E000101") { ?>
                                                                         style="display: block; color: red; font-weight: bold;" <?php } else { ?>
                                                                         style="display: none;" <?php } ?>>
                                                                         <?php echo REPEATED_JOURNAL; ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="col-md-4 control-label"><?php echo JOURNAL_TITLE; ?><span
                                                                            class="required"> * </span></label>
                                                                    <div class="col-md-6">
                                                                        <input type="text" class="form-control "
                                                                               name="title" value="<?php echo $title ?>">
                                                                        <span id="title_tooltip"
                                                                              class="tooltiptext" style="display: none; color:red"><?php echo FILL_THIS_FIELD; ?></span>

                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="col-md-4 control-label"><?php echo JOURNAL_FIELD; ?><span
                                                                            class="required"> * </span></label>
                                                                    <div class="col-md-6">
                                                                        <input type="text" class="form-control "
                                                                               id = "field" name="field" value="<?php echo $field ?>">
                                                                        <span id="field_tooltip"
                                                                              class="tooltiptext" style="display: none; color:red"><?php echo FILL_THIS_FIELD; ?></span>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="col-md-4 control-label"><?php echo JOURNAL_ABSRANK; ?> </label>
                                                                    <div class="col-md-6">
                                                                        <input type="text" act="yes" class="form-control"
                                                                               name="abs_rank" value="<?php echo $abs_rank ?>"
                                                                               id="abs_rank">

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="col-md-4 control-label"><?php echo JOURNAL_DOMICILE; ?></label>
                                                                    <div class="col-md-6">
                                                                        <select class="form-control " name="domicile"
                                                                                id="domicile" class="form-control">
                                                                                    <?php
                                                                                    if ($domicile_list) {
                                                                                        for ($i = 0; $i < count($domicile_list); $i ++) {
                                                                                            $domicile_id_s = $domicile_list [$i] ["id"];
                                                                                            $domicile_name_s = $domicile_list [$i] ["name"];
                                                                                            ?>
                                                                                    <option value="<?= $domicile_id_s ?>"
                                                                                    <?php if ($domicile_name_s == $domicile_name) { ?>
                                                                                                selected="selected" <?php } ?>> <?= $domicile_name_s ?></option>
                                                                                            <?php
                                                                                        }
                                                                                    } else {
                                                                                        ?>
                                                                                <option value="" selected></option>
                                                                                <?php
                                                                            }
                                                                            ?>		
                                                                        </select>
                                                                        <span id="domicile_name_tooltip" class="tooltiptext"
                                                                              style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="col-md-4 control-label"><?php echo JOURNAL_IMPACT_FACTOR; ?> </label>
                                                                    <div class="col-md-6">
                                                                        <input type="text" name="impact_factor" class="form-control no-validate"
                                                                               value="<?php echo $impact_factor ?>" id="impact_factor" act="yes"
                                                                               onfocus="javascript:removeTooltip('impact_factor_tooltip');">
                                                                        <span id="impact_factor_tooltip" class="tooltiptext"
                                                                              style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="col-md-4 control-label"><?php echo JOURNAL_ISSN; ?> </label>
                                                                    <div class="col-md-6">
                                                                        <input type="text" name="issn" class="form-control no-validate"
                                                                               value="<?php echo $issn ?>" id="issn"
                                                                               onfocus="javascript:removeTooltip('issn_tooltip');">
                                                                        <span id="issn_tooltip" class="tooltiptext"
                                                                              style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="col-md-4 control-label"><?php echo JOURNAL_INTR; ?> </label>
                                                                    <div class="col-md-6">
                                                                        <input type="text" name="intr" act="yes" class="form-control"
                                                                               value="<?php echo $intr ?>" id="intr"
                                                                               onfocus="javascript:removeTooltip('intr_tooltip');">
                                                                        <span id="intr_tooltip" class="tooltiptext"
                                                                              style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="col-md-4 control-label"><?php echo JOURNAL_STAFF_VIEWS_ADVICE; ?> </label>
                                                                    <div class="col-md-6">
                                                                        <input type="text" name="staff_views_advice" class="form-control no-validate"
                                                                               value="<?php echo $staff_views_advice ?>" id="staff_views_advice"
                                                                               onfocus="javascript:removeTooltip('staff_views_advice_tooltip');">
                                                                        <span id="staff_views_advice_tooltip" class="tooltiptext"
                                                                              style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="col-md-4 control-label"><?php echo JOURNAL_FREQUENCY; ?> </label>
                                                                    <div class="col-md-6">
                                                                        <input type="text" name="articles_num" act="yes" class="form-control"
                                                                               value="<?php echo $articles_num ?>" id="articles_num"
                                                                               onfocus="javascript:removeTooltip('articles_num_tooltip');">
                                                                        <span id="articles_num_tooltip" class="tooltiptext"
                                                                              style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="col-md-4 control-label"><?php echo FINANCIAL_TIMES_RANKING; ?> </label>
                                                                    <div class="col-md-6">
                                                                        <table style="width: 60%"
                                                                               onfocus="javascript:removeTooltip('financial_times_ranking');">
                                                                            <tr>
                                                                                <td><input type="radio" name="financial_times_ranking"
                                                                                           id="financial_times_ranking" value="1"
                                                                                           <?php if ($financial_times_ranking == '1') { ?> checked
                                                                                           <?php } ?>></td>
                                                                                <td><?php echo YES; ?></td>
                                                                                <td><input type="radio" name="financial_times_ranking"
                                                                                           id="financial_times_ranking" value="0"
                                                                                           <?php if ($financial_times_ranking == '0') { ?> checked
                                                                                           <?php } ?>></td>
                                                                                <td><?php echo NO; ?></td>
                                                                            </tr>
                                                                        </table>
                                                                        <span id="financial_times_ranking_tooltip" class="tooltiptext"
                                                                              style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row"> 
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="col-md-4 control-label"><?php echo LAST_UPDATE_DATE; ?> </label>
                                                                    <div class="col-md-6">
                                                                        <input disabled="true" name = "last_update_date" type="date" class="form-control"
                                                                               value="<?php echo $last_update_date ?>">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label class="col-md-2 control-label"><?php echo URL; ?> </label>
                                                                    <div class="col-md-9">
                                                                        <input type="text" name="url" act="yes" class="form-control"
                                                                               value="<?php echo $url ?>" id="url"
                                                                               onfocus="javascript:removeTooltip('url_tooltip');">
                                                                        <span id="url_tooltip" class="tooltiptext"
                                                                              style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-actions">
                                                            <div class="row">
                                                                <div class="col-md-offset-3 col-md-9">
                                                                    <button type="submit" class="btn btn-circle blue"><?php
                                                                        if ($id) {
                                                                            echo EDIT;
                                                                        } else {
                                                                            echo INSERT;
                                                                        }
                                                                        ?></button>
                                                                    <a
                                                                        href="<?= site_url('admin_panel/getJournalsListView/1') ?>"
                                                                        class="btn btn-outline btn-circle grey-salsa"><?php echo CANCEL; ?>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>

                                                </form>
                                                <!-- END FORM-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php $this->load->view('utils/footer'); ?>

    </body>

</html>