<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<?php
session_start();
$this->load->view('localization/lang');
$this->load->view('localization/txt');
defineLocale();
defineStrings();
?>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <script type="text/javascript">
            function checkValid(email_valdation) {
                var user_name = document.forms["myForm"]["user_name"].value;
                var password = document.forms["myForm"]["password"].value;
                var retyped_password = document.forms["myForm"]["retyped_password"].value;
                var name = document.forms["myForm"]["name"].value;


                if (name == "") {
                    $("#name_tooltip").css("display", "block");
                    $("#name_tooltip").css("visibility", "visible");
                    return false;
                } else {
                    $("#name_tooltip").css("display", "none");
                    $("#name_tooltip").css("visibility", "none");
                }
                if (user_name == "") {
                    $("#user_name_tooltip").css("display", "block");
                    $("#user_name_tooltip").css("visibility", "visible");
                    return false;
                } else {
                    $("#user_name_tooltip").css("display", "none");
                    $("#user_name_tooltip").css("visibility", "none");
                }
                if (password == "") {
                    $("#password_tooltip").css("display", "block");
                    $("#password_tooltip").css("visibility", "visible");
                    return false;
                } else {
                    $("#password_tooltip").css("display", "none");
                    $("#password_tooltip").css("visibility", "none");
                }
                if (retyped_password == "" || password != retyped_password) {
                    $("#retyped_password_tooltip").css("display", "block");
                    $("#retyped_password_tooltip").css("visibility", "visible");
                    return false;
                } else {
                    $("#retyped_password_tooltip").css("display", "none");
                    $("#retyped_password_tooltip").css("visibility", "none");
                }
            }
        </script>
        <meta charset="utf-8" />
        <title><?php echo TITLE; ?></title>
		<link href="<?= base_url() ?>css/journals_custom_css.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url() ?>assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
    </head>
    <!-- END HEAD -->
    <body class="page-container-bg-solid">
        <div class="page-wrapper">
            <?php include 'general_admin_header.php'; ?>
            <br>
            <div class="page-wrapper-row" style="height: auto !important;">
                <div class="page-wrapper-middle" style="background-color: #FFF !important;">
                    <!-- BEGIN CONTAINER -->
                    <div class="page-container">
                        <!-- BEGIN CONTENT -->
                        <div class="page-content-wrapper">
                            <!-- BEGIN CONTENT BODY -->
                            <!-- BEGIN PAGE HEAD-->
                            <div class="page-head">
                                <div class="container">
                                    <div class="col-md-12">
                                        <div class="portlet box grey-mint">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-gift"></i>
                                                    <?php
                                                    if ($id) {
                                                        echo $name;
                                                    } else {
                                                        echo ADD_RECORD;
                                                    }
                                                    ?>
                                                </div>

                                            </div>
                                            <div class="portlet-body form">
                                                <!-- BEGIN FORM-->
                                                <form id="myForm" autocomplete="off" role="form"
                                                      method="post"
                                                      onsubmit="javascript:return checkValid('<?php echo INVALID_EMAIL; ?>');"
                                                      action="<?php if ($id) {
                                                        echo site_url('admin_panel/updateUser/' . $id);
                                                    } else {
                                                        echo site_url('admin_panel/createUser/');
                                                    }
                                                    ?>"
                                                      class="form-horizontal">
                                                    <div class="form-body">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group" <?php if (isset($code) && $code == "E000102") { ?>
                                                                         style="display: block; color: red; font-weight: bold;" <?php } else { ?>
                                                                         style="display: none;" <?php } ?>>
                                                                         <?php echo REPEATED_UNIVERSITY_ID_NUMBER; ?>
                                                                </div>
                                                                <div class="form-group" <?php if (isset($code) && $code == "E000101") { ?>
                                                                         style="display: block; color: red; font-weight: bold;" <?php } else { ?>
                                                                         style="display: none;" <?php } ?>>
                                                                         <?php echo REPEATED_USER; ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="col-md-5 control-label"><?php echo ID; ?><span
                                                                            class="required"> * </span></label>
                                                                    <div class="col-md-6">
                                                                        <input type="text" act="yes" class="form-control "
                                                                               name="id_number" value="<?php echo $id_number ?>">
                                                                        <span id="id_number_tooltip"
                                                                              class="tooltiptext" style="display: none; color:red"><?php echo FILL_THIS_FIELD; ?></span>

                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="col-md-5 control-label"><?php echo NAME; ?><span
                                                                            class="required"> * </span></label>
                                                                    <div class="col-md-6">
                                                                        <input type="text" class="form-control "
                                                                               name="name" value="<?php echo $name ?>">
                                                                        <span id="name_tooltip"
                                                                              class="tooltiptext" style="display: none; color:red"><?php echo FILL_THIS_FIELD; ?></span>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="col-md-5 control-label"><?php echo USERNAME; ?> <span
                                                                            class="required"> * </span></label>
                                                                    <div class="col-md-6">
                                                                        <input type="text" class="form-control no-validate"
                                                                               name="user_name" value="<?php echo $user_name ?>"
                                                                               id="user_name"
                                                                               onfocus="javascript:removeTooltip('user_name_tooltip');">
                                                                        <span id="user_name_tooltip" class="tooltiptext"
                                                                                  <?php if (isset($username_exists_error) && $username_exists_error == 1) { ?>
                                                                                  style="display: block; color: red;" <?php } else { ?>
                                                                                  style="display: none; color:red;" <?php } ?>><?php
                                                                                  if (isset($username_exists_error) && $username_exists_error == 1) {
                                                                                      echo USERNAME_IS_EXIST;
                                                                                  } else {
                                                                                      echo FILL_THIS_FIELD;
                                                                                  }
                                                                                  ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="col-md-5 control-label"><?php echo PASSWORD; ?><span
                                                                            class="required"> * </span></label>
                                                                    <div class="col-md-6">
                                                                        <input type="password" class="form-control no-validate"
                                                                               name="password" value="<?php echo $password ?>"
                                                                               id="password"
                                                                               onfocus="javascript:removeTooltip('password_tooltip');">
                                                                        <span id="password_tooltip" class="tooltiptext"
                                                                              style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="col-md-5 control-label"><?php echo REPEAT_PASSWORD; ?> <span
                                                                            class="required"> * </span></label>
                                                                    <div class="col-md-6">
                                                                        <input type="password" class="form-control no-validate"
                                                                               value="<?php echo $password ?>" id="retyped_password"
                                                                               onfocus="javascript:removeTooltip('retyped_password_tooltip');">
                                                                        <span id="retyped_password_tooltip" class="tooltiptext"
                                                                              style="display: none; color: red"><?php echo INCORRECT_REPEATED_PASSWORD; ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-actions">
                                                            <div class="row">
                                                                <div class="col-md-offset-3 col-md-9">
                                                                    <button type="submit" class="btn btn-circle blue"><?php
                                                                        if ($id) {
                                                                            echo EDIT;
                                                                        } else {
                                                                            echo INSERT;
                                                                        }
                                                                        ?></button>
                                                                    <a
                                                                        href="<?= site_url('admin_panel/getUsersListView/1') ?>"
                                                                        class="btn btn-outline btn-circle grey-salsa"><?php echo CANCEL; ?>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>

                                                </form>
                                                <!-- END FORM-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php $this->load->view('utils/footer'); ?>
        </div>

    </body>

</html>