<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
<meta charset="utf-8" />
<title><?php echo TITLE; ?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta
	content="Preview page of Metronic Admin RTL Theme #3 for dashboard & statistics"
	name="description" />
<meta content="" name="author" />
<script>
$(document).ready(function(){
    $("#delBtn").click(function(){
        $("#delNotify").show();
    });
    $("#cancel").click(function(){
        $("#delNotify").hide();
    });
    $("#delNotify").hide();

	$(".marker-class").on("click", function() {
		$('#loading').removeClass('hidden');
	}); 
});
</script>

</head>
<!-- END HEAD -->

<body class="page-container-bg-solid">
	<div id="loading" class="hidden">
	  <img id="loading-image" src="<?php echo base_url(); ?>images/Ajax-loader2.gif" alt="<?php echo LOADING;?>" />
	</div>
		<?php $this->load->view ( 'utils/admin_header' );?>
		<!-- BEGIN HEADER -->
			<div class="page-header">	
				<!-- BEGIN HEADER MENU -->
				<div class="page-header-menu">
					<div class="container">
						<div class="hor-menu  ">
							<ul class="nav navbar-nav">
							<?php if (isset($users) || isset($user_name)) {?>
								<li aria-haspopup="true"
									class="menu-dropdown classic-menu-dropdown active">
								<?php } else {?>	
								<li aria-haspopup="true"
									class="menu-dropdown classic-menu-dropdown">
									<?php } ?>
									<a class="marker-class"
									href="<?=site_url('admin_panel/getUsersListView')?>"> Users <span
										class="arrow"></span>
								</a></li>
								<?php if (isset($journals)|| isset($title)) {?>
								<li aria-haspopup="true"
									class="menu-dropdown classic-menu-dropdown active">
								<?php } else {?>	
								<li aria-haspopup="true"
									class="menu-dropdown classic-menu-dropdown">
									<?php } ?>
								<a class="marker-class"
									href="<?=site_url('admin_panel/getJournalsListView')?>"> Journals <span
										class="arrow"></span>
								</a></li>
																
								<?php if (isset($comments) || isset($description)) {?>
								<li aria-haspopup="true"
									class="menu-dropdown classic-menu-dropdown active">
								<?php } else {?>	
								<li aria-haspopup="true"
									class="menu-dropdown classic-menu-dropdown">
									<?php } ?><a class="marker-class"
									href="<?=site_url('admin_panel/view_comments')?>"> Comments <span class="arrow"></span>
								</a></li>
								
							</ul>
						</div>
						<!-- END MEGA MENU -->
					</div>
				</div>
				<!-- END HEADER MENU -->
                        </div>
		
</body>