<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<?php
session_start();
$this->load->view('localization/lang');
$this->load->view('localization/txt');
defineLocale();
defineStrings();
?>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
<meta charset="utf-8" />
<title><?php echo TITLE; ?></title>
<link rel="shortcut icon" href="<?=base_url()?>images/shortcut-icon.png" />
<script	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<link href="<?php echo base_url()?>css/custom_popup.css" rel="stylesheet" type="text/css">
<link href="<?= base_url() ?>css/journals_custom_css.css" rel="stylesheet" type="text/css" />
<link href="<?= base_url() ?>assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
 
</head>
<!-- END HEAD -->
<body class="page-container-bg-solid">

	<div class="page-wrapper">
            <?php include 'general_admin_header.php'; ?>
            <div class="page-wrapper-row full-height">

			<!-- BEGIN CONTAINER -->
			<div class="page-container">
				<!-- BEGIN CONTENT -->
				<div class="page-content-wrapper">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <div class="container">
                            <!-- BEGIN PAGE TITLE -->
                            <div class="page-title">
                                <h1><?php echo Comments; ?></h1>
                            </div>
                            <!-- END PAGE TITLE -->
                        </div>
                    </div>
                    <!-- END PAGE HEAD-->
					<!-- BEGIN CONTENT BODY -->
					<div class="container">
						<!-- BEGIN BORDERED TABLE PORTLET-->
						<div id="all-page" class="row">
							<div class="col-md-12">
								<div class="portlet-body">
									<div id="pending-comments" class="table-scrollable"></div>

									<div id="pagination_div"
										class="search-pagination pagination-rounded"
										style="display: none; margin: auto; text-align: center;">
										<div id="pagination_form">
											<input type="hidden" name="current_page" id="current_page"
												value="1"> <input type="button" class="bold btn dark"
												style="width: 9rem !important;" id="prev_button"
												onClick="javascript:get_prev_page()"
												value="<?php echo PREVIOUS ;?>" /> <label><?php echo PAGE_NUM; ?>:</label>
											<input id="desired_page_num" type="number"
												class="pagination-input" value="1" min="1" />/ <span
												id="total_pages_num"></span> <input type="button"
												class="bold btn dark" style="width: 9rem !important;"
												id="next_button" onClick="javascript:get_next_page()"
												value="<?php echo NEXT ;?>" />
										</div>
									</div>
									<br>
								</div>
								<!-- END BORDERED TABLE PORTLET-->
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
        <?php $this->load->view('utils/footer'); ?>
        
    <script type="text/javascript">
		function approveComment(comment_id){
			changeCommentStatus(comment_id, 'A');
		}

		function rejectComment(comment_id){
			changeCommentStatus(comment_id, 'R');
		}

		function changeCommentStatus(comment_id, action){
			var admin_id = "<?php echo $this->session->userdata ( 'adminid' ); ?>";
          	var url = "<?php echo site_url('Comments_controller/executeCommentAction'); ?>";
          	$.ajax({
			      type: 'POST',
			      url: url,
			      data: {
			    	  admin_id: admin_id,
			    	  comment_id: comment_id,
			    	  action: action
              	  },
            	  beforeSend: function(){
            			$('#loading').removeClass('hidden');
            	  },
			      success: function(data){
	                   var results = JSON.parse(data);
	                   console.log(results);
	                   var status_code = results.code;
	                   if(status_code == 'I000000'){
	    			   		var modal = document.getElementById('popup_modal');
	    					var span = document.getElementsByClassName("close")[0];
	    					span.onclick = function() {
	    						$(modal).fadeIn().delay(0).fadeOut();
	    					}
	    					window.onclick = function(event) {
	    					    if (event.target == modal) {
	    					    	$(modal).fadeIn().delay(0).fadeOut();
	    					    }
	    					}
	    					if(action == 'A'){
	    						document.getElementById("message").innerHTML = "<?php echo APPROVE_EXECUTED_SUCCESSFULLY; ?>";
	    					}else if(action == 'R'){
	    						document.getElementById("message").innerHTML = "<?php echo REJECTED_EXECUTED_SUCCESSFULLY; ?>";
	    					}
	    					$(modal).fadeIn().delay(1000).fadeOut();
		                	//refresh table 
	                      	var current_page = parseInt($("#current_page").val()) ;
	                       	get_pending_comments(current_page);
	                   }
			      },
	              complete : function(data){
	            	  $('#loading').addClass('hidden');
	              },
				  error: function(){      
					   alert('Error while request..');
				  }
    		});
		}

	  jQuery(document).ready(function() {
    	    get_pending_comments(1);
          	$(document).on('keyup','#desired_page_num',function(event) { 
    		    if(event.keyCode == 13){
    		    	var newpage  = $('#desired_page_num').val();
    		    	get_pending_comments(newpage);
    		    }
    		});
	   });

	  
	   function get_pending_comments(newpage){
    		 $('#loading').removeClass('hidden');
    		 var newpage = adjust_pagination(newpage);
    		 var page_size = 20;
    		 var url = "<?php echo site_url('admin_panel/getPendingCommentsList'); ?>"+'/'+page_size+'/'+newpage;
        	 $.ajax({
        		 type: 'GET',
        		 url: url,
        		 cache: false,
        		 async: false,
    		 	 complete : function(data){
    		     	console.log(data);
    		        $('html, body').animate({
    		            scrollTop: $("#all-page").offset().top
    		        }, 500); 
    		     	$('#loading').addClass('hidden');
    		 	 },
        		 success: function(json){      
    	    		  try{  
    	        		  	console.log(json);
							display_pending_comments(json, page_size);
    	    		  }catch(e) {  
    	    			  console.log('Exception while request..');
    	    		  }  
        		  },
        		  error: function(){      
        			  console.log('Error while request..');
        		  }
           });
    		 
	   }

       function get_prev_page (){
           	var current_page = parseInt($("#current_page").val()) ;
           	var newpage = (current_page-1);
           	get_pending_comments(newpage);
        }

        function get_next_page(){
           	var current_page = parseInt($("#current_page").val()) ;
           	var newpage = (current_page+1);
           	get_pending_comments(newpage);
        }

        function adjust_pagination(newpage){
			var no_of_pages = $("#total_pages_num").text();
			if(no_of_pages != ""){
    			if (parseInt(newpage) >= parseInt(no_of_pages)){
    				newpage = no_of_pages;
    				$("#next_button").prop('disabled', true);
    			}else{
    				$("#next_button").prop('disabled', false);
    			}
			}else{
				newpage = 1;
			}
			if (parseInt(newpage) > 1){
				$("#prev_button").prop('disabled', false);
			}else{
				$("#prev_button").prop('disabled', true);
			}
			$("#current_page").val(newpage);
			$("#desired_page_num").val(newpage);
			return newpage;
        }

        function display_pending_comments(data, page_size){
        	var no_of_pages = $("#total_pages_num").text();
        	var newpage = $("#desired_page_num").val();
            var results = JSON.parse(data);
            //console.log(results);
            var status_code = results.code;
            if(status_code == 'I000000'){
                var total_count = results.total_count;
                var pending_comments = results.pending_comments;
                //console.log('total_count= '+total_count+' , journals = '+journals);
                var new_html = "<table class='table table-bordered table-hover comments-table'><thead><tr><th><?php echo JOURNAL_TITLE; ?></th><th><?php echo COMMENT_DATE; ?></th><th><?php echo USERNAME; ?></th><th><?php echo DESCRIPTION; ?></th><th></th></tr></thead><tbody>";
                var rows = '';

				if( parseInt(total_count) > 0){
                    var curr_page_count = 0;
                    for (var key in pending_comments) {
                        //console.log(pending_comments[key]);
                        var record = pending_comments[key];
                        var comment_id = record.comment_id;
                        var journal_title = '';
                        if (record.journal_title && record.journal_title != null) {
                        	journal_title = record.journal_title;
                        }
                        var comment_date = record.comment_date;
                        var user_name = record.user_name;
                        var description = record.description;

                        var row = "<tr><td style='width: 200px;'>"+journal_title+"</td><td style='width: 155px;'>"+comment_date+"</td><td style='width: 150px;'>"+user_name+"</td><td style='max-width: 250px;min-width: 200px;'><div>"+description+"</div></td><td style='width: 150px;'>";
                        row = row + "<table class='actions-table'><tr><td><button class='btn green bold btn-block' onclick='javascript:approveComment("+comment_id+")'><?php echo APPROVE;?></button></td><td><button class='btn red bold btn-block' onclick='javascript:rejectComment("+comment_id+")'><?php echo REJECT;?></button></td></tr></table></td></tr>";
                        //console.log(row);
                        rows = rows + row;
                        curr_page_count= curr_page_count+1;
                    }
                    
                    if(total_count > page_size){
                    	$('#pagination_div').css("display","block"); 
                    	var number_of_pages = Math.ceil(total_count / page_size);
                    	$("#desired_page_num").prop('max',number_of_pages);
                    	$("#desired_page_num").attr('max',number_of_pages);
                    	$("#total_pages_num").text(number_of_pages);
            			if (parseInt(newpage) >= parseInt(number_of_pages)){
            				$("#next_button").prop('disabled', true);
            			}else{
            				$("#next_button").prop('disabled', false);
            			}
            			if (parseInt(newpage) > 1){
            				$("#prev_button").prop('disabled', false);
            			}else{
            				$("#prev_button").prop('disabled', true);
            			}
                    }
				}else{
					rows = "<tr class='HeadBr2'><td colspan='22' style='color: red' align='center'><h4><?php echo NO_RECORDS_FOUND; ?></h4></td></tr>";
                	$('#pagination_div').css("display","none");
                }
				new_html = new_html + rows;
				new_html = new_html + "</tbody</table>";
				$("#pending-comments").html(new_html);
                $('#loading').addClass('hidden');
            }else{
            	$('#loading').addClass('hidden');
            	$('#pagination_div').css("display","none");
            	$("#journal_search_results").html('');
            }

        }
	   
    </script>

	<div id="popup_modal" class="popup">
		<div class="popup-content">
			<span class="close">&times;</span>
			<div class="page-container"
				style="font-size: 16px; margin: auto; text-align: center;">
				<img alt="" src="<?php echo base_url()?>images/sucess.png">
				<span id="message" style="color: green;"></span>
			</div>
		</div>
	</div>
	
	
</body>

</html>