<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<?php
$this->load->view('localization/lang');
$this->load->view('localization/txt');
$this->load->helper('url');
defineLocale();
defineStrings();
?>
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>

        <!-- Resources -->
        <script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
        <script src="https://www.amcharts.com/lib/3/serial.js"></script>
        <script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
        <link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
        <script src="https://www.amcharts.com/lib/3/themes/light.js"></script>

        <script>
            window.onload = function () {
                $('#loading').removeClass('hidden');
                loadDomicileList();
                loadFieldsList();
                loadABSRankList();
                loadSliders();
                drawChart();
                $('#loading').addClass('hidden');
                $('#fire_change_listener').val('1');
            };
        </script>

        <link rel="shortcut icon" href="<?= base_url() ?>images/shortcut-icon.png" />

    </head>
    <!-- END HEAD -->

    <body class="page-container-bg-solid">
        <div class="page-wrapper">
            <?php $this->load->view('utils/main_header'); ?>

            <input type="hidden" id="current_search" value="">

            <input type="hidden" id="min_articles_number" value="">
            <input type="hidden" id="max_articles_number" value="">
            <input type="hidden" id="fire_change_listener" value="0">

            <!-- BEGIN HEADER -->
            <div class="page-header">
                <!-- BEGIN HEADER MENU -->
                <div class="page-header-menu">
                    <div class="container">
                        <!-- BEGIN HEADER SEARCH BOX -->
                        <div id="quick_search_form" class="search-form" >
                            <div class="input-group">
                                <input type="text" class="form-control no-validate" placeholder="Search"
                                       id="searchTextId" name="searchTextId" /> <span class="input-group-btn"> <a
                                        href="#" id="quick_search_anchor"
                                        class="btn submit"> <i
                                            class="icon-magnifier"></i>
                                    </a>
                                </span>
                            </div>
                        </div>
                        <!-- END HEADER SEARCH BOX -->
                        <!-- BEGIN MEGA MENU -->
                        <!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
                        <!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
                        <div class="hor-menu  ">
                            <ul class="nav navbar-nav">
                                <li aria-haspopup="true" id="advanced_search" onclick="apply_quick_search('', '');"
                                    class="menu-dropdown classic-menu-dropdown active"><a
                                        href="javascript:;"> <?php echo ADVANCED_SEARCH; ?> <span class="arrow"></span>
                                    </a></li>
                                <li aria-haspopup="true" 
                                    class="menu-dropdown classic-menu-dropdown"><a
                                        href="javascript:;"> About the tool <span class="arrow"></span>
                                    </a></li>
                                <li aria-haspopup="true" id="fav_list"
                                    class="menu-dropdown classic-menu-dropdown"><a
                                        href="javascript:;" onclick="get_fav_list(1);"> <?php echo FAV_JOURNALS; ?> <span class="arrow"></span>
                                    </a></li>
                            </ul>
                        </div>
                        <!-- END MEGA MENU -->
                    </div>
                </div>
                <!-- END HEADER MENU -->
            </div>

            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <!-- BEGIN CONTAINER -->
                    <div class="page-container">
                        <!-- BEGIN CONTENT -->
                        <div class="page-content-wrapper">
                            <!-- BEGIN CONTENT BODY -->
                            <!-- BEGIN PAGE HEAD-->
                            <div class="page-head">
                                <div class="container">
                                    <!-- BEGIN PAGE TITLE -->
                                    <div class="page-title">
                                        <h1><?php echo ADVANCED_SEARCH; ?></h1>
                                    </div>
                                    <!-- END PAGE TITLE -->
                                </div>
                            </div>
                            <!-- END PAGE HEAD-->
                            <!-- BEGIN PAGE CONTENT BODY -->
                            <div class="page-content">
                                <div class="container">
                                    <!-- BEGIN PAGE CONTENT INNER -->
                                    <div class="page-content-inner">
                                        <div class="search-page search-content-3">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="search-filter ">
                                                        <div id="advancedSearchResultsCountDiv" class="search-label font-red hidden" style="float: right;">
                                                            <?php echo ADVANCED_SEARCH_RESULT_COUNT; ?>
                                                            <span id="advancedSearchResultsCount" class="search-count-class"></span>
                                                            <?php echo JOURNALS; ?>
                                                        </div>
                                                        <div class="search-label uppercase"><?php echo JOURNAL_TITLE; ?></div>
                                                        <div class="input-icon right">
                                                            <i class="icon-magnifier"></i>
                                                            <input id="journal_title_id" type="text" class="form-control no-validate" placeholder="Filter by keywords"> </div>
                                                        <div class="search-label uppercase"><?php echo JOURNAL_FIELD; ?></div>
                                                        <select 
                                                            name="select_field[]"
                                                            id="select_field"
                                                            class="form-control select2-multiple" 
                                                            multiple="multiple">
                                                        </select>
                                                        <div class="search-label uppercase"><?php echo JOURNAL_ABSRANK; ?></div>
                                                        <select 
                                                            name="select_absRank[]"
                                                            id="select_absRank"
                                                            class="form-control select2-multiple"
                                                            multiple="multiple">
                                                        </select>
                                                        <div class="search-label uppercase"><?php echo JOURNAL_DOMICILE; ?></div>
                                                        <select 
                                                            name="multiple_domicile[]"
                                                            id="multiple_domicile"
                                                            class="form-control select2-multiple"
                                                            multiple="multiple">
                                                        </select>

                                                        <div class="search-label uppercase"><?php echo JOURNAL_FREQUENCY; ?></div>
                                                        <div id="demo9" class="noUi-danger"></div>
                                                        <input type="hidden" id="articles_number_from" value="">
                                                        <input type="hidden" id="articles_number_to" value="">
                                                        <button id="execute_adv_search" class="btn red bold uppercase btn-block" onclick="javascript:apply_advanced_search('1', 'newSearch')">
                                                            <?php echo ADVANCED_SEARCH_BUTTON; ?>
                                                        </button>
                                                        <div class="row">
                                                            <div class="col-xs-6 col-xs-offset-3">
                                                                <button class="btn grey bold uppercase btn-block" style="margin-top: 0px !important;" onclick="javascript:reset_advanced_search()">
                                                                    <?php echo CLEAR_ADVANCED_SEARCH_BUTTON; ?>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div id="result_search" class="col-lg-8">
                                                    <div id="loading" class="hidden">
                                                        <img id="loading-image" src="<?php echo base_url(); ?>images/Ajax-loader2.gif" alt="<?php echo LOADING; ?>" />
                                                    </div>
                                                    <div id="more_info_panel" class="hidden" 
                                                         style="background: #fff; padding: 20px;">
                                                    </div>
                                                    <div id="no_search_results" class="font-red bold hidden center-item" style="background: #fff; padding: 20px;"><?php echo NO_SEARCH_RESULTS; ?></div>
                                                    <div id="chartdivContainer" style="background: #fff; padding: 20px;">
                                                        <div class="portlet-title">
                                                            <div class="caption">
                                                                <span class="caption-subject font-dark bold uppercase">Journal Fields Statistics </span>
                                                                <small class="font-red">Chart displays number of journals per field</small>
                                                            </div>
                                                        </div>
                                                        <div class="container-outer">
                                                            <div class="container-inner">
                                                                <div id="chartdiv" class="CSSAnimationChart"></div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div id="journal_search_results" class="row">
                                                    </div>
                                                    <div id="pagination_div" class="search-pagination pagination-rounded" style="display: none;">
                                                        <div id = "pagination_form" >
                                                            <input type="hidden" name="current_page" id="current_page" value="1">
                                                            <input type="button" class="bold btn dark" style="width: 9rem !important;"
                                                                   id="prev_button"
                                                                   onClick="javascript:get_prev_page()" value="<?php echo PREVIOUS; ?>" />
                                                            <label><?php echo PAGE_NUM; ?>:</label>
                                                            <input id="desired_page_num" type="number" class="pagination-input" value="1" min="1" />/ <span id="total_pages_num"></span>
                                                            <input type="button" class="bold btn dark" style="width: 9rem !important;"
                                                                   id="next_button"
                                                                   onClick="javascript:get_next_page()" value="<?php echo NEXT; ?>" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END PAGE CONTENT INNER -->
                                </div>
                            </div>
                            <!-- END PAGE CONTENT BODY -->
                            <!-- END CONTENT BODY -->
                        </div>
                        <!-- END CONTENT -->
                    </div>
                    <!-- END CONTAINER -->
                </div>
            </div>
            <?php $this->load->view('utils/footer.php'); ?>

        </div>

        <script>
            $(document).ready(function () {

                $(document).on('keyup', '#desired_page_num', function (event) {
                    if (event.keyCode == 13) {
                        var newpage = $('#desired_page_num').val();
                        var current_search = $("#current_search").val();
                        if (current_search == 'quick') {
                            apply_quick_search(newpage, '');
                        } else if (current_search == 'fav') {
                            get_fav_list(newpage);
                        } else {
                            apply_advanced_search(newpage, '');
                        }
                    }
                });

                $('#quick_search_form').on('keyup keypress', function (e) {
                    var keyCode = e.keyCode || e.which;
                    if (keyCode === 13) {
                        e.preventDefault();
                        return false;
                    }
                });

                $("#searchTextId").keyup(function (event) {
                    if (event.keyCode == 13) {
                        apply_quick_search('1', 'newSearch');
                    }
                });

                $("#quick_search_anchor").click(function (e) {
                    e.preventDefault();//this will prevent the link trying to navigate to another page
                    var href = $(this).attr("href");
                    apply_quick_search('1', 'newSearch');
                    return false;
                });

                $('#journal_title_id').on('keyup keydown keypress', function (e) {
                    var new_val = $(e.target).val();
                    //console.log(new_val);
                    display_search_results_count("");
                });

                $('.search-filter').on('keyup keydown keypress', function (e) {
                    var keyCode = e.keyCode || e.which;
                    if (keyCode === 13) {
                        apply_advanced_search('1', 'newSearch');
                    }
                });

                $('#select_field').change(function (e) {
                    var selected = $(e.target).val();
                    //console.log(selected);
                    display_search_results_count("field");
                });

                $('#select_absRank').change(function (e) {
                    var selected = $(e.target).val();
                    //console.log(selected);
                    display_search_results_count("abs_rank");
                });

                $('#multiple_domicile').change(function (e) {
                    var selected = $(e.target).val();
                    //console.log(selected);
                    display_search_results_count("domicile");
                });

                $('#multiple_domicile').select2({
                    placeholder: "<?php echo SELECT_DOMICILES; ?>"
                });

                $('#select_field').select2({
                    placeholder: "<?php echo SELECT_FIELD; ?>"
                });

                $('#select_absRank').select2({
                    placeholder: "<?php echo SELECT_ABSRANK; ?>"
                });


                $(document).on('keyup', '#comments_desired_page_num', function (event) {
                    if (event.keyCode == 13) {
                        var user_id = "<?php echo $this->session->userdata('userid'); ?>";
                        var current_journal_id = $("#current_journal_id").val()
                        var newpage = $('#comments_desired_page_num').val();
                        newpage = adjust_comments_pagination(newpage);
                        var comments_html = get_comments_html(user_id, current_journal_id, newpage);
                        $("#comments_div").html(comments_html);
                        $('#loading').addClass('hidden');
                    }
                });

            });

            function get_prev_page() {
                var current_page = parseInt($("#current_page").val());
                var newpage = (current_page - 1);
                var current_search = $("#current_search").val();
                if (current_search == 'quick') {
                    apply_quick_search(newpage, '');
                } else if (current_search == 'fav') {
                    get_fav_list(newpage);
                } else {
                    apply_advanced_search(newpage, '');
                }
            }

            function get_next_page() {
                var current_page = parseInt($("#current_page").val());
                var newpage = (current_page + 1);
                var current_search = $("#current_search").val();
                if (current_search == 'quick') {
                    apply_quick_search(newpage, '');
                } else if (current_search == 'fav') {
                    get_fav_list(newpage);
                } else {
                    apply_advanced_search(newpage, '');
                }
            }
            var result_page_data = null;
            var page_size = 8;
            function apply_quick_search(newpage, newSearch) {
                $("#current_search").val("quick");
                var element = document.getElementById("fav_list");

                if (element.classList.contains("active"))
                    element.classList.remove("active");

                var element = document.getElementById("advanced_search");

                if (!element.classList.contains("active"))
                    element.className += " active";
                var searchTextVal = $('#searchTextId').val();
                if (searchTextVal == '') {
                    $("#journal_search_results").html('');
                    $('#pagination_div').css("display", "none");
                    $('#no_search_results').addClass('hidden');
                    $('#chartdivContainer').css("display", "block");
                    drawChart();
                    return;
                }
                newpage = adjust_search_before(newpage, newSearch, page_size);
                if (searchTextVal.length > 1) {
                    searchTextVal = searchTextVal.replace("&", "and");
                }
                var url = "<?php echo site_url('search_journals/executeBasicSearch'); ?>" + "/" + encodeURIComponent(searchTextVal) + '/' + page_size + '/' + newpage;
                $.ajax({
                    type: 'GET',
                    url: url,
                    dataType: 'json',
                    success: function (data) {
                        //alert("Data: " + data + "\nStatus: " + status);
                        result_page_data = data;
                        display_search_results(data, page_size);
                    },
                    error: function () {
                        //alert('Error while request..');
                    }
                });
            }
            function apply_advanced_search(newpage, newSearch) {
                $("#current_search").val("advanced");
                var element = document.getElementById("fav_list");

                if (element.classList.contains("active"))
                    element.classList.remove("active");

                var element = document.getElementById("advanced_search");

                if (!element.classList.contains("active"))
                    element.className += " active";
                var page_size = 8;
                newpage = adjust_search_before(newpage, newSearch, page_size);
                var url = "<?php echo site_url('search_journals/executeAdvancedSearch'); ?>" + '/' + page_size + '/' + newpage;
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: {
                        title: get_advanced_search_title(),
                        field: get_advanced_search_select_field(),
                        abs_rank: get_advanced_search_select_absRank(),
                        domicile_ids: get_advanced_search_select_domicile(),
                        articles_number_from: get_advanced_search_articles_number_from(),
                        articles_number_to: get_advanced_search_articles_number_to()
                    },
                    dataType: 'json',
                    beforeSend: function () {
                        $('#loading').removeClass('hidden');
                    },
                    success: function (data) {
                        //alert("Data: " + data + "\nStatus: " + status);
                        result_page_data = data;
                        display_search_results(data, page_size);
                        display_search_results_count("");
                    },
                    error: function () {
                        //alert('Error while request..');
                    }
                });

            }
            function get_fav_list(newpage) {
                $("#current_search").val("fav");
                var element = document.getElementById("advanced_search");

                if (element.classList.contains("active"))
                    element.classList.remove("active");

                var element = document.getElementById("fav_list");

                if (!element.classList.contains("active"))
                    element.className += " active";

                newpage = adjust_search_before(newpage, 'newSearch', page_size);
                var url = "<?php echo site_url('search_journals/getUserFavoritesJournals'); ?>" + '/' + page_size + '/' + newpage;
                console.log(url);
                $.ajax({
                    type: 'GET',
                    url: url,
                    dataType: 'json',
                    success: function (data) {
                        console.log(data);
                        result_page_data = data;
                        display_search_results(data, page_size);
                    },
                    error: function () {
                        alert('Error while request..');
                    }
                });
            }
            function reset_advanced_search() {
                $('#advancedSearchResultsCountDiv').addClass('hidden');
                $('#loading').removeClass('hidden');
                $('#fire_change_listener').val('0');
                $("#journal_title_id").val('');
                $("#select_field").val('').trigger('change');
                $("#select_absRank").val('').trigger('change');
                $('#multiple_domicile').val('').trigger('change');
                $("#articles_number_from").val($("#min_articles_number").val());
                $("#articles_number_to").val($("#max_articles_number").val());
                var articles_numberSlider = document.getElementById('demo9');
                articles_numberSlider.noUiSlider.set([$("#articles_number_from").val(), $("#articles_number_to").val()]);
                $('#more_info_panel').addClass('hidden');
                $('#no_search_results').addClass('hidden');
                $('#pagination_div').css("display", "none");
                $("#journal_search_results").html('');
                $('#chartdivContainer').css("display", "block");
                $('#fire_change_listener').val('1');
                $('#loading').addClass('hidden');
                loadDomicileList();
                loadFieldsList();
                loadABSRankList();
                drawChart();
            }

            function in_list(str, list) {
                var i, len;
                if (list && list != null) {
                    for (i = 0, len = list.length; i < len; i++) {
                        var areEqual = (list[i].toUpperCase() === str.toUpperCase());
                        if (areEqual) {
                            return true;
                        }
                    }
                }
                return false;
            }

            function get_advanced_search_title() {
                var journal_title_val = $("#journal_title_id").val();
                if (!journal_title_val || journal_title_val == '') {
                    journal_title_val = "null";
                }
                return journal_title_val;
            }

            function get_advanced_search_select_field() {
                var select_field_arr = $("#select_field").val();
                var all_exists = in_list('ALL', select_field_arr);
                var select_field_val = '';
                if (select_field_arr == null || all_exists) {
                    select_field_val = "null";
                } else {
                    select_field_val = select_field_arr.join('_');
                    if (!select_field_val || select_field_val == '') {
                        select_field_val = "null";
                    }
                }
                return select_field_val;
            }

            function get_advanced_search_select_absRank() {
                var select_absRank_arr = $("#select_absRank").val();
                var select_absRank_val = '';
                if (select_absRank_arr == null) {
                    select_absRank_val = "null";
                } else {
                    select_absRank_val = select_absRank_arr.join();
                    if (!select_absRank_val || select_absRank_val == '') {
                        select_absRank_val = "null";
                    }
                }
                return select_absRank_val;
            }

            function get_advanced_search_select_domicile() {
                var multiple_domicile_arr = $("#multiple_domicile").val();
                var multiple_domicile_val = '';
                if (multiple_domicile_arr == null) {
                    multiple_domicile_val = "null";
                } else {
                    multiple_domicile_val = multiple_domicile_arr.join();
                    if (!multiple_domicile_val || multiple_domicile_val == '') {
                        multiple_domicile_val = "null";
                    }
                }
                return multiple_domicile_val;
            }

            function get_advanced_search_articles_number_from() {
                var articles_number_from_val = $("#articles_number_from").val();
                return articles_number_from_val;
            }

            function get_advanced_search_articles_number_to() {
                var articles_number_to_val = $("#articles_number_to").val();
                return articles_number_to_val;
            }

            function adjust_search_before(newpage, newSearch, page_size) {
                $('#loading').removeClass('hidden');
                $('#more_info_panel').addClass('hidden');
                $("#journal_search_results").removeClass('hidden');
                $('#pagination_div').removeClass('hidden');
                $('#no_search_results').addClass('hidden');
                $('#chartdivContainer').css("display", "none");
                if (newSearch == 'newSearch') {
                    $("#journal_search_results").html('');
                    $('#pagination_div').css("display", "none");
                }
                var no_of_pages = $("#total_pages_num").text();
                if (no_of_pages != "") {
                    if (parseInt(newpage) >= parseInt(no_of_pages)) {
                        newpage = no_of_pages;
                        $("#next_button").prop('disabled', true);
                    } else {
                        $("#next_button").prop('disabled', false);
                    }
                } else {
                    newpage = 1;
                }
                if (parseInt(newpage) > 1) {
                    $("#prev_button").prop('disabled', false);
                } else {
                    $("#prev_button").prop('disabled', true);
                }
                $("#current_page").val(newpage);
                $("#desired_page_num").val(newpage);
                return newpage;
            }

            function display_search_results(results, page_size) {
                var newpage = $("#desired_page_num").val();
                //console.log(results);
                var status_code = results.code;
                if (status_code == 'I000000') {
                    var total_count = results.total_count;
                    var journals = results.journals;
                    //console.log('total_count= '+total_count+' , journals = '+journals);

                    if (parseInt(total_count) > 0) {
                        var curr_page_count = 0;
                        var new_html = '';
                        for (var key in journals) {
                            //console.log(journals[key]);
                            var journal_id = key.replace("journal_id", "");
                            var record = journals[key];
                            var abs_rank = record.abs_rank;
                            var ribbon_color = "ribbon-color-default";
                            if (abs_rank && abs_rank >= 0) {
                                if (abs_rank == "4") {
                                    ribbon_color = "ribbon-color-danger";
                                } else if (abs_rank == "3") {
                                    ribbon_color = "ribbon-color-warning";
                                } else if (abs_rank == "2") {
                                    ribbon_color = "ribbon-color-success";
                                } else if (abs_rank == "1") {
                                    ribbon_color = "ribbon-color-info";
                                } else if (abs_rank == "0") {
                                    ribbon_color = "ribbon-color-primary";
                                }
                            } else {
                                abs_rank = '&ensp;';
                            }
                            var title = record.title;
                            if (title.length > 75)
                            {
                                title = title.substring(0, 75) + "...";
                            }
                            var field_val = " ";
                            var domicile_val = " ";
                            var impact_factor_val = " ";
                            var frequency_val = " ";
                            var articles_num_val = " ";
                            if (record.field && record.field != null) {
                                field_val = record.field;
                            }
                            if (record.domicile_name && record.domicile_name != null) {
                                domicile_val = record.domicile_name;
                            }
                            if (record.impact_factor && record.impact_factor != null) {
                                impact_factor_val = record.impact_factor;
                            }
                            if (record.frequency && record.frequency != null) {
                                frequency_val = record.frequency;
                            }
                            if (record.articles_num && record.articles_num != null) {
                                articles_num_val = record.articles_num;
                            }
                            var recordBody = "<b><?php echo JOURNAL_FIELD; ?>:</b> " + field_val + ", <b><?php echo JOURNAL_DOMICILE; ?>:</b> " + domicile_val + "<br/>";
                            recordBody = recordBody + "<b><?php echo JOURNAL_FREQUENCY; ?>:</b> " + articles_num_val + "<br/>";
                            recordBody = recordBody + "<button class='button-as-link' onclick='javascript:display_journal_details(" + journal_id + ")' ><?php echo MORE_INFO; ?></button>";
                            var star_form = "<div class='rating'><input type='checkbox' id='star_" + journal_id + "' name='rating' ";
                            if (record.user_favorites_id && record.user_favorites_id != null) {
                                star_form = star_form + "checked";
                            }
                            star_form = star_form + " onchange='javascript:update_user_journal_rate(this, " + journal_id + ")'/><label for='star_" + journal_id + "' title=''></label></div>";
                            var row = "<div class='col-md-6'><div class='portlet mt-element-ribbon light portlet-fit '><div class='ribbon ribbon-vertical-right ribbon-shadow ";
                            row = row + ribbon_color;
                            row = row + " uppercase'><div class='ribbon-sub ribbon-bookmark'>" + star_form + "</div><div style = 'font-weight: bold; color: #fff'>" + abs_rank + "</div></div>";
                            row = row + "<div class='portlet-title'><div class='caption custom-caption'><i class=' icon-layers font-red'></i><span class='caption-subject font-red bold uppercase'>" + title + "</span>";
                            row = row + "</div></div><div class='portlet-body journal-block'>";
                            row = row + recordBody;
                            row = row + "</div></div></div>";
                            //console.log(row);
                            new_html = new_html + row;
                            curr_page_count = curr_page_count + 1;
                        }

                        $("#journal_search_results").html(new_html);

                        if (total_count > page_size) {
                            $('#pagination_div').css("display", "block");
                            var number_of_pages = Math.ceil(total_count / page_size);
                            $("#desired_page_num").prop('max', number_of_pages);
                            $("#desired_page_num").attr('max', number_of_pages);
                            $("#total_pages_num").text(number_of_pages);
                            if (parseInt(newpage) >= parseInt(number_of_pages)) {
                                $("#next_button").prop('disabled', true);
                            } else {
                                $("#next_button").prop('disabled', false);
                            }
                            if (parseInt(newpage) > 1) {
                                $("#prev_button").prop('disabled', false);
                            } else {
                                $("#prev_button").prop('disabled', true);
                            }
                        }
                    } else {
                        $('#pagination_div').css("display", "none");
                        $("#journal_search_results").html('');
                        $('#chartdivContainer').css("display", "none");
                        $('#no_search_results').removeClass('hidden');
                    }
                    $('#loading').addClass('hidden');
                } else {
                    $('#loading').addClass('hidden');
                    $('#pagination_div').css("display", "none");
                    $("#journal_search_results").html('');
                    $('#chartdivContainer').css("display", "block");
                    drawChart();
                }

                $('html, body').animate({
                    scrollTop: $("#result_search").offset().top
                }, 500);
            }

            function update_user_journal_rate(checkboxElem, journal_id) {
                // for update the star state in list after changing it in "More Info"
                var fav_journal = result_page_data.journals["journal_id" + journal_id]["user_favorites_id"];
                if (fav_journal == null) {
                    result_page_data.journals["journal_id" + journal_id]["user_favorites_id"] = "1";
                } else {
                    result_page_data.journals["journal_id" + journal_id]["user_favorites_id"] = null;
                }
                console.log(result_page_data.journals["journal_id" + journal_id]["user_favorites_id"]);
                var checked = checkboxElem.checked;
                console.log(checked);
                var url = "";
                if (checkboxElem.checked) {
                    url = "<?php echo site_url('User_Favorites_controller/createUserFavoriteJournal'); ?>";
                } else {
                    url = "<?php echo site_url('User_Favorites_controller/deleteUserFavoriteJournal'); ?>";
                }
                var user_id = "<?php echo $this->session->userdata('userid'); ?>";
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: {
                        user_id: user_id,
                        journal_id: journal_id
                    },
                    dataType: 'json',
                    success: function (data) {
                        var results = data;
                        console.log(results);
                        var status_code = results.code;
                        if (status_code == 'I000000') {
                            // update fav list
                            var current_state = $("#current_search").val();
                            if (current_state == "fav") {
                                setTimeout(get_fav_list(1), 3000);
                            }
                        }
                    },
                    error: function (request, status, error) {
                        alert("There is Server Error");
                    }
                });

            }

            function display_journal_details(journal_id) {
                $('#loading').removeClass('hidden');
                var url = "<?php echo site_url('search_journals/getJournalDetail'); ?>" + "/" + encodeURIComponent(journal_id);
                var backButton = "<input type='button' class='back-button' onclick='javascript:back()' title='Back'/>";

                $.ajax({
                    type: 'GET',
                    url: url,
                    success: function (data) {
                        //console.log("Data: " + data + "\nStatus: " + status);
                        var results = JSON.parse(data);
                        //console.log(results);
                        var status_code = results.code;
                        if (status_code == 'I000000') {
                            var journal_detail = (results.journal_detail);
                            //console.log(journal_detail);
                            var abs_rank = journal_detail.abs_rank;
                            var ribbon_color = "ribbon-color-default";
                            if (abs_rank && abs_rank >= 0) {
                                if (abs_rank == "4") {
                                    ribbon_color = "ribbon-color-danger";
                                } else if (abs_rank == "3") {
                                    ribbon_color = "ribbon-color-warning";
                                } else if (abs_rank == "2") {
                                    ribbon_color = "ribbon-color-success";
                                } else if (abs_rank == "1") {
                                    ribbon_color = "ribbon-color-info";
                                } else if (abs_rank == "0") {
                                    ribbon_color = "ribbon-color-primary";
                                }
                            } else {
                                abs_rank = '&ensp;';
                            }
                            var title = journal_detail.title;
                            var field_val = " ";
                            var domicile_val = " ";
                            var impact_factor_val = " ";
                            var frequency_val = " ";
                            var issn_val = " ";
                            var abs_2015_val = " ";
                            var intr_val = " ";
                            var sjr_val = " ";
                            var articles_num_val = " ";
                            var staff_views_advice_val = " ";
                            var url_val = " ";
                            var ajg_2018_val = " ";
                            var ajg_2015_val = " ";
                            var abs_2010_val = " ";
                            var abs_2009_val = " ";
                            var jcr_rank_val = " ";
                            var sjr_rank_val = " ";
                            var snip_rank_val = " ";
                            var ipp_rank_val = " ";
                            var articles_num_val = " ";
                            var last_update_date = " ";
                            if (journal_detail.field && journal_detail.field != null) {
                                field_val = journal_detail.field;
                            }
                            if (journal_detail.domicile_name && journal_detail.domicile_name != null) {
                                domicile_val = journal_detail.domicile_name;
                            }
                            if (journal_detail.impact_factor && journal_detail.impact_factor != null) {
                                impact_factor_val = journal_detail.impact_factor;
                            }
                            if (journal_detail.frequency && journal_detail.frequency != null) {
                                frequency_val = journal_detail.frequency;
                            }
                            if (journal_detail.issn && journal_detail.issn != null) {
                                issn_val = journal_detail.issn;
                            }
                            if (journal_detail.abs_2015 && journal_detail.abs_2015 != null) {
                                abs_2015_val = journal_detail.abs_2015;
                            }
                            if (journal_detail.intr && journal_detail.intr != null) {
                                intr_val = journal_detail.intr;
                            }
                            if (journal_detail.sjr && journal_detail.sjr != null) {
                                sjr_val = journal_detail.sjr;
                            }
                            if (journal_detail.articles_num && journal_detail.articles_num != null) {
                                articles_num_val = journal_detail.articles_num;
                            }
                            if (journal_detail.staff_views_advice && journal_detail.staff_views_advice != null) {
                                staff_views_advice_val = journal_detail.staff_views_advice;
                            }
                            if (journal_detail.url && journal_detail.url != null) {
                                url_val = journal_detail.url;
                            }
                            if (journal_detail.ajg_2018 && journal_detail.ajg_2018 != null) {
                                ajg_2018_val = journal_detail.ajg_2018;
                            }
                            if (journal_detail.ajg_2015 && journal_detail.ajg_2015 != null) {
                                ajg_2015_val = journal_detail.ajg_2015;
                            }
                            if (journal_detail.abs_2010 && journal_detail.abs_2010 != null) {
                                abs_2010_val = journal_detail.abs_2010;
                            }
                            if (journal_detail.abs_2009 && journal_detail.abs_2009 != null) {
                                abs_2009_val = journal_detail.abs_2009;
                            }
                            if (journal_detail.jcr_rank && journal_detail.jcr_rank != null) {
                                jcr_rank_val = journal_detail.jcr_rank;
                            }
                            if (journal_detail.sjr_rank && journal_detail.sjr_rank != null) {
                                sjr_rank_val = journal_detail.sjr_rank;
                            }
                            if (journal_detail.snip_rank && journal_detail.snip_rank != null) {
                                snip_rank_val = journal_detail.snip_rank;
                            }
                            if (journal_detail.ipp_rank && journal_detail.ipp_rank != null) {
                                ipp_rank_val = journal_detail.ipp_rank;
                            }
                            if (journal_detail.last_update_date && journal_detail.last_update_date != null) {
                                last_update_date = journal_detail.last_update_date;
                            }
                            if (journal_detail.articles_num && journal_detail.articles_num != null) {
                                articles_num_val = journal_detail.articles_num;
                            }
                            var recordBody = "<table class='journal-details' >";
                            recordBody = recordBody + "<tr><td><b><?php echo JOURNAL_ISSN; ?>:</b> " + issn_val + "</td>";
                            recordBody = recordBody + "<td><b><?php echo JOURNAL_FIELD; ?>:</b> " + field_val + "</td></tr>";
                            recordBody = recordBody + "<tr><td><b><?php echo JOURNAL_DOMICILE; ?>:</b> " + domicile_val + "</td>";
                            //recordBody = recordBody + "<td><b><?php echo JOURNAL_IMPACT_FACTOR; ?>:</b> "+ impact_factor_val + "</td></tr>";
                            recordBody = recordBody + "<td><div class='freq-detail'><b><?php echo JOURNAL_FREQUENCY; ?>:</b> " + articles_num_val + "</div> </td></tr>";
                            //recordBody = recordBody + "<td><b><?php echo FINANCIAL_TIMES_RANKING; ?>:</b>";
                            //	   if (journal_detail.financial_times_ranking && journal_detail.financial_times_ranking != '0'){recordBody = recordBody + " Yes </td>";}else{recordBody = recordBody + " No </td>";}
                            // recordBody = recordBody + "</tr>";
                            // recordBody = recordBody + "<tr><td><b><?php echo JOURNAL_ABS_2015; ?>:</b> "+ abs_2015_val + "</td>";
                            recordBody = recordBody + "<tr><td><b><?php echo JOURNAL_INTR; ?>:</b> " + intr_val + "</td>";
                            //recordBody = recordBody + "<tr><td><b><?php echo JOURNAL_SJR; ?>:</b> "+ sjr_val + "</td>";
                            //recordBody = recordBody + "<td><b><?php echo JOURNAL_ARTICLES_NUM; ?>:</b> "+ articles_num_val + "</td></tr>";
                            //recordBody = recordBody + "<tr><td><b><?php echo JOURNAL_AJG_2018; ?>:</b> "+ ajg_2018_val + "</td>";
                            //recordBody = recordBody + "<td><b><?php echo JOURNAL_AJG_2015; ?>:</b> "+ ajg_2015_val + "</td></tr>";
                            //recordBody = recordBody + "<tr><td><b><?php echo JOURNAL_ABS_2010; ?>:</b> "+ abs_2010_val + "</td>";
                            //recordBody = recordBody + "<td><b><?php echo JOURNAL_ABS_2009; ?>:</b> "+ abs_2009_val + "</td></tr>";
                            //recordBody = recordBody + "<tr><td><b><?php echo JOURNAL_JCR_RANK; ?>:</b> "+ jcr_rank_val + "</td>";
                            //recordBody = recordBody + "<td><b><?php echo JOURNAL_SJR_RANK; ?>:</b> "+ sjr_rank_val + "</td></tr>";
                            //recordBody = recordBody + "<tr><td><b><?php echo JOURNAL_SNIP_RANK; ?>:</b> "+ snip_rank_val + "</td>";
                            //recordBody = recordBody + "<td><b><?php echo JOURNAL_IPP_RANK; ?>:</b> "+ ipp_rank_val + "</td></tr>";
                            recordBody = recordBody + "<td><b><?php echo JOURNAL_ABS_2018; ?>:</b> " + abs_rank + "</td></tr>";
                            recordBody = recordBody + "<tr><td><b><?php echo LAST_UPDATE_DATE; ?>:</b> " + last_update_date + "</td></tr>";
                            recordBody = recordBody + "<tr><td colspan='2'><b><?php echo JOURNAL_STAFF_VIEWS_ADVICE; ?>:</b> " + staff_views_advice_val + "</td></tr>";
                            recordBody = recordBody + "<tr><td colspan='2'><b><?php echo URL; ?>:</b><a target='_blank' rel='noopener noreferrer' href='"+ url_val +"'>"+ url_val +"</a></td></tr>";
                            recordBody = recordBody + "</table>";
                            var star_form = "<div class='rating'><input type='checkbox' id='detail_star_" + journal_id + "' name='rating' ";
                            if (journal_detail.user_favorites_id && journal_detail.user_favorites_id != null) {
                                star_form = star_form + "checked";
                            }
                            star_form = star_form + " onchange='javascript:update_user_journal_rate(this, " + journal_id + ")'/><label for='detail_star_" + journal_id + "' title=''></label></div>";
                            var row = "<div ><div class='portlet mt-element-ribbon light portlet-fit '><div class='ribbon ribbon-vertical-right ribbon-shadow ";
                            row = row + ribbon_color;
                            row = row + " uppercase'><div class='ribbon-sub ribbon-bookmark'>" + star_form + "</div><div style = 'font-weight: bold'>" + abs_rank + "</div></div><div class='portlet-title'>" + backButton;
                            row = row + "<div class='caption'><i class=' icon-layers font-red'></i><span class='caption-subject font-red bold uppercase'>" + title + "</span></div></div><div class='portlet-body'>";
                            row = row + recordBody;
                            row = row + "</div></div></div>";
                            //console.log(row);
                            var comments_html = '';
                            var user_id = "<?php echo $this->session->userdata('userid'); ?>";
                            if (user_id != '' && user_id != null) {
                                comments_html = get_comments_html(user_id, journal_id, 1);
                            }
                            var final_html = row + "<div id='comments_div'>" + comments_html + "</div>";
                            $('#more_info_panel').html(final_html);
                            $("#journal_search_results").addClass('hidden');
                            $('#pagination_div').addClass('hidden');
                            $('#more_info_panel').removeClass('hidden');
                            $('#loading').addClass('hidden');
                            $('html, body').animate({
                                scrollTop: $("#result_search").offset().top
                            }, 500);
                        }
                    },
                    error: function () {
                        //alert('Error while request..');
                    }
                });
            }

            function back() {
                console.log(result_page_data);
                $('#loading').removeClass('hidden');
                $('#more_info_panel').addClass('hidden');
                $("#journal_search_results").removeClass('hidden');
                $('#pagination_div').removeClass('hidden');
                var newpage = $('#current_page').val();
                var current_search = $("#current_search").val();
                if (current_search == 'quick' || current_search == 'fav') {
                    display_search_results(result_page_data, page_size);
                } else {
                    display_search_results(result_page_data, page_size);
                    display_search_results_count("");
                }
                $('#loading').addClass('hidden');
            }

            function drawChart() {

                var url = "<?php echo site_url('journals_meta_data/getJournalsChart'); ?>";
                $.ajax({
                    type: 'GET',
                    url: url,
                    success: function (data) {
                        //console.log("Data: " + data + "\nStatus: " + status);
                        var results = JSON.parse(data);
                        //console.log(results);
                        var status_code = results.code;
                        if (status_code == 'I000000') {
                            var fields_count = results.fields_count;
                            initAmChart(fields_count);
                        }
                    },
                    error: function () {
                        //alert('Error while request..');
                    }
                });

            }

            function initAmChart(chart_array) {
                if (typeof (AmCharts) === 'undefined' || $('#chartdiv').size() === 0) {
                    return;
                }
                var chart = AmCharts.makeChart("chartdiv", {
                    "type": "serial",
                    "theme": "light",
                    "rotate": true,
                    "dataProvider": chart_array,
                    "valueAxes": [{
                            "gridColor": "#FFFFFF",
                            "gridAlpha": 0.2,
                            "dashLength": 0,
                            "title": "<?php echo JOURNALS_COUNT; ?>"
                        }],
                    "gridAboveGraphs": true,
                    "startDuration": 1,
                    "graphs": [{
                            "balloonText": "[[category]]: <b>[[value]]</b>",
                            "fillAlphas": 0.8,
                            "lineAlpha": 0.2,
                            "lineColor": "#E01C20",
                            "type": "column",
                            "valueField": "journals_count",
                            "minHorizontalGap": 150
                        }],
                    "chartCursor": {
                        "categoryBalloonEnabled": false,
                        "cursorAlpha": 0,
                        "zoomable": false
                    },
                    "categoryField": "journals_field",
                    "categoryAxis": {
                        "gridPosition": "start",
                        "gridAlpha": 0,
                        "ignoreAxisWidth": true,
                        "fontSize": 9,
                        "autoGridCount": false,
                        "gridCount": 50,
                        "title": "Journal Fields",
                        "autoWrap": true

                    },
                    "marginLeft": 250

                });

            }

            function loadDomicileList() {
                $('#multiple_domicile').empty();
                var url = "<?php echo site_url('advanced_search_form_data/getDomicileList'); ?>";

                $.ajax({
                    type: 'GET',
                    url: url,
                    async: false,
                    success: function (data) {
                        //console.log("Data: " + data + "\nStatus: " + status);
                        var results = JSON.parse(data);
                        //console.log(results);
                        var status_code = results.code;
                        if (status_code == 'I000000') {
                            var domicileList = results.domicileList;
                            var domicileListCount = results.domicileListCount;
                            fillListValues(domicileList, domicileListCount, $('#multiple_domicile'));
                        }
                    },
                    error: function () {
                        //alert('Error while request..');
                    }
                });
            }

            function loadFieldsList() {
                var url = "<?php echo site_url('advanced_search_form_data/getFieldsList'); ?>";
                $.ajax({
                    type: 'GET',
                    url: url,
                    async: false,
                    success: function (data) {
                        //console.log("Data: " + data + "\nStatus: " + status);
                        var results = JSON.parse(data);
                        //console.log(results);
                        var status_code = results.code;
                        if (status_code == 'I000000') {
                            var fieldsListCount = results.fieldsListCount;
                            var fieldsList = results.fieldsList;
                            fillListValues(fieldsList, fieldsListCount, $('#select_field'));

                        }
                    },
                    error: function () {
                        //alert('Error while request..');
                    }
                });
            }

            function loadABSRankList() {
                $('#select_absRank').empty();
                var url = "<?php echo site_url('advanced_search_form_data/getABSRankList'); ?>";
                $.ajax({
                    type: 'GET',
                    url: url,
                    async: false,
                    success: function (data) {
                        //console.log("Data: " + data + "\nStatus: " + status);
                        var results = JSON.parse(data);
                        //console.log(results);
                        var status_code = results.code;
                        if (status_code == 'I000000') {
                            var ABSRankListCount = results.ABSRankListCount;
                            var ABSRankList = results.ABSRankList;
                            fillListValues(ABSRankList, ABSRankListCount, $('#select_absRank'));

                        }
                    },
                    error: function () {
                        //alert('Error while request..');
                    }
                });

            }

            function fillListValues(list, listCount, select) {
                var multiple_vals = select.val();
                select.empty();
                if (listCount > 0) {
                    for (var key in list) {
                        var record = list[key];
                        var name = record.name;
                        if (multiple_vals && (multiple_vals.includes(name) || multiple_vals.includes(key))) {
                            select.append($('<option selected></option>').attr('value', key).text(name));
                        } else {
                            select.append($('<option></option>').attr('value', key).text(name));
                        }

                    }
                }

            }
            var articlesNumMin = 0;
            var articleNumMax = 0;
            var manualChange = false;
            function loadSliders() {
                var min_articles_number = 0;
                var max_articles_number = 0;
                var url1 = "<?php echo site_url('advanced_search_form_data/getArticlesNumberBoundaries'); ?>";
                $.ajax({
                    type: "get",
                    url: url1,
                    async: false,
                    success: function (data) {
                        try {
                            var results = JSON.parse(data);
                            //console.log(results);
                            var status_code = results.code;
                            if (status_code == 'I000000') {
                                var ArticlesNumberBoundaries = results.ArticlesNumberBoundaries;
                                min_articles_number = parseInt(Math.floor(ArticlesNumberBoundaries.min_articles_number));
                                max_articles_number = parseInt(Math.ceil(ArticlesNumberBoundaries.max_articles_number));
                                articlesNumMin = min_articles_number;
                                articleNumMax = max_articles_number;
                            }
                            //alert( 'Success');
                        } catch (e) {
                            //alert('Exception while request..');
                        }
                    },
                    error: function () {
                        //alert('Error while request..');
                    }
                });



                ComponentsNoUiSliders.init(min_articles_number, max_articles_number);
                $("#articles_number_from").val(min_articles_number);
                $("#articles_number_to").val(max_articles_number);
                $("#min_articles_number").val(min_articles_number);
                $("#max_articles_number").val(max_articles_number);

                var articles_numberSlider = document.getElementById('demo9');
                articles_numberSlider.noUiSlider.on("update", function (values, handle) {
                    //console.log(values); //array such as  Array [ "15.79", "38.05" ] , length=2
                    //console.log(handle);//0 means min changes , 1 means max changed
                    manualChange = true;
                    $("#articles_number_from").val(parseInt(values[0]));
                    $("#articles_number_to").val(parseInt(values[1]));
                    //console.log('demo9');
                    document.getElementById("execute_adv_search").focus();
                    display_search_results_count("article");
                });

            }

            function display_search_results_count(action) {
                var fire_change_listener = $('#fire_change_listener').val();
                //console.log('fire_change_listener= '+ $('#fire_change_listener').val());
                if (fire_change_listener == '1') {
                    var url = "<?php echo site_url('search_journals/getAdvancedSearchResultsCount'); ?>";
                    var title = get_advanced_search_title();
                    var field = get_advanced_search_select_field();
                    var abs_rank = get_advanced_search_select_absRank();
                    var domicile_ids = get_advanced_search_select_domicile();
                    var articles_number_from = get_advanced_search_articles_number_from();
                    var articles_number_to = get_advanced_search_articles_number_to();
                    if (manualChange == false || articles_number_from == null || articles_number_to == null) {
                        articles_number_from = articlesNumMin;
                        articles_number_to = articleNumMax;
                    }
                    console.log({
                        title: title,
                        field: field,
                        abs_rank: abs_rank,
                        domicile_ids: domicile_ids,
                        articles_number_from: articles_number_from,
                        articles_number_to: articles_number_to
                    });
                    $.ajax({

                        url: url,
                        type: 'POST',
                        data: {
                            title: title,
                            field: field,
                            abs_rank: abs_rank,
                            domicile_ids: domicile_ids,
                            articles_number_from: articles_number_from,
                            articles_number_to: articles_number_to
                        },
                        dataType: 'json',
                        success: function (data) {
                            var results = data;
                            console.log(results);
                            var status_code = results.code;
                            if (status_code == 'I000000') {
                                var total_count = results.total_count;
                                var domicileList = results.domicile_list;
                                var domicileCount = results.domicile_count;
                                var fieldsList = results.fields_list;
                                var fieldCount = results.field_count;
                                var ABSRankList = results.absRank_list;
                                var absRankCount = results.absRank_count;
                                var min_articles_number = results.min_articles_number;
                                var max_articles_number = results.max_articles_number;
                                $("#advancedSearchResultsCount").text(total_count);
                                $('#advancedSearchResultsCountDiv').removeClass('hidden');
                                if (action !== "domicile")
                                    fillListValues(domicileList, domicileCount, $('#multiple_domicile'));
                                if (action !== "field")
                                    fillListValues(fieldsList, fieldCount, $('#select_field'));
                                if (action !== "abs_rank")
                                    fillListValues(ABSRankList, absRankCount, $('#select_absRank'));
                                if (action !== "article" && (min_articles_number != $("#articles_number_from").val()
                                        || max_articles_number != $("#articles_number_to").val())) {
                                    var articles_numberSlider = document.getElementById('demo9');
                                    articles_numberSlider.noUiSlider.set([min_articles_number, max_articles_number]);
                                    manualChange = false;
                                    $("#journal_title_id").focus();
                                }
                            }
                        },
                        error: function (request, status, error) {
                            //alert(error);
                        }
                    });
                }
            }

            function commentKeyUp() {
                var comment = document.getElementById("new_comment");
                var commentSubmit = document.getElementById("comment_submit");
                if (/\S/.test(comment.value)) {
                    commentSubmit.disabled = false;
                } else {
                    commentSubmit.disabled = true;
                }
            }

            function get_comments_html(user_id, journal_id, page_num) {
                var all_html = '';
                var comments_list_html = '';
                var page_size = 10;
                var has_pagination = false;
                var prev_button_disabled = false;
                var next_button_disabled = false;
                var comments_total_pages_num = '';
                var url = "<?php echo site_url('Comments_controller/getJournalCommentsList'); ?>" + "/" + encodeURIComponent(user_id) + "/" + encodeURIComponent(journal_id) + "/" + encodeURIComponent(page_size) + "/" + encodeURIComponent(page_num);
                $.ajax({
                    type: 'GET',
                    url: url,
                    async: false,
                    success: function (data) {
                        var results = JSON.parse(data);
                        //console.log(results);
                        var status_code = results.code;
                        if (status_code == 'I000000') {
                            var total_count = results.total_count;
                            if (parseInt(total_count) > 0) {
                                commentList = results.commentList;
                                for (var key in commentList) {
                                    //console.log( key, commentList[ key ] );
                                    var record = commentList[key];
                                    var user_name = record.user_name;
                                    var description = record.description;
                                    var comment_date = record.comment_date;
                                    var desc_class = '';
                                    if (record.status == 'A') {
                                        desc_class = "approved";
                                    } else if (record.status == 'P') {
                                        desc_class = "pending";
                                    }
                                    comments_list_html = comments_list_html + "<div class='form-group'><div class='col-md-11'><div class='commenter-style'>" + user_name + "<small class='comment-date'><i>  " + comment_date + "</i></small>";
                                    if (record.status == 'P') {
                                        comments_list_html = comments_list_html + "<span class='comment-status'><?php echo PENDING_FOR_ADMIN_APPROVAL; ?></span>";
                                    }
                                    comments_list_html = comments_list_html + "</div><div class='comment-style " + desc_class + "'>" + description + "</div></div></div>";
                                }

                                if (total_count > page_size) {
                                    has_pagination = true;
                                    comments_total_pages_num = Math.ceil(total_count / page_size);
                                    if (parseInt(page_num) >= parseInt(comments_total_pages_num)) {
                                        next_button_disabled = true;
                                    } else {
                                        next_button_disabled = false;
                                    }
                                    if (parseInt(page_num) > 1) {
                                        prev_button_disabled = false;
                                    } else {
                                        prev_button_disabled = true;
                                    }
                                }

                            } else {
                                comments_list_html = comments_list_html + "<div class='form-group'></div>";
                            }
                        }
                    },
                    error: function () {
                        //alert('Error while request..');
                    }
                });

                var form_html = "<div class='form-group'><div class='col-md-9'>";
                form_html = form_html + " <textarea class='form-control ' rows='2' onkeyup='commentKeyUp()' placeholder='<?php echo WRITE_COMMENT; ?>' id='new_comment' name='new_comment' ></textarea>";
                form_html = form_html + " </div><div class='col-md-2'>";
                form_html = form_html + " <button disabled='disabled' id='comment_submit' onclick='javascript:add_journal_comment(" + user_id + "," + journal_id + ")' class='btn red bold uppercase btn-block'><?php echo SUBMIT; ?></button></div></div>";

                var comments_pagination = "";
                if (has_pagination) {
                    comments_pagination = "<div id='comments_pagination_div' class='search-pagination pagination-rounded'>";
                    comments_pagination = comments_pagination + "<div id = 'comments_pagination_form'><input type='hidden' id='comments_current_page' value='" + page_num + "'>";
                    comments_pagination = comments_pagination + "<input type='hidden' id='current_journal_id' value='" + journal_id + "'>";
                    comments_pagination = comments_pagination + "<input type='button' class='bold btn dark' style='width: 9rem !important;' id='comments_prev_button' onClick='javascript:get_comments_prev_page()' value='<?php echo PREVIOUS; ?>' ";
                    if (prev_button_disabled) {
                        comments_pagination = comments_pagination + "disabled";
                    }
                    comments_pagination = comments_pagination + " /><span> </span><label><?php echo PAGE_NUM; ?>:</label><input id='comments_desired_page_num' type='number' class='pagination-input' value='" + page_num + "' min='1' max='" + comments_total_pages_num + "'/> /";
                    comments_pagination = comments_pagination + " <span id='comments_total_pages_num'>" + comments_total_pages_num + "</span><span> </span>";
                    comments_pagination = comments_pagination + "<input type='button' class='bold btn dark' style='width: 9rem !important;' id='comments_next_button' onClick='javascript:get_comments_next_page()' value='<?php echo NEXT; ?>' ";
                    if (next_button_disabled) {
                        comments_pagination = comments_pagination + "disabled";
                    }
                    comments_pagination = comments_pagination + " /></div></div>";
                }

                all_html = "<div><div class='portlet box'><div><div class='caption-subject font-red bold uppercase'><?php echo COMMENTS; ?></div></div><br><div class='portlet-body form form-horizontal'>";
                all_html = all_html + form_html + comments_list_html + comments_pagination + "</div></div></div>";
                return all_html;
            }

            function add_journal_comment(user_id, journal_id) {
                var new_comment = $("#new_comment").val();
                var url = "<?php echo site_url('Comments_controller/addJournalComment'); ?>";
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: {
                        user_id: user_id,
                        journal_id: journal_id,
                        description: new_comment
                    },
                    beforeSend: function () {
                        $('#loading').removeClass('hidden');
                    },
                    success: function (data) {
                        var results = JSON.parse(data);
                        //console.log(results);
                        var status_code = results.code;
                        if (status_code == 'I000000') {
                            display_journal_details(journal_id);
                        }
                    },
                    complete: function (data) {
                    },
                    error: function () {
                        //alert('Error while request..');
                    }
                });
            }

            function get_comments_prev_page() {
                var user_id = "<?php echo $this->session->userdata('userid'); ?>";
                var current_journal_id = $("#current_journal_id").val()
                var comments_current_page = parseInt($("#comments_current_page").val());
                var newpage = (comments_current_page - 1);
                newpage = adjust_comments_pagination(newpage);
                var comments_html = get_comments_html(user_id, current_journal_id, newpage);
                $("#comments_div").html(comments_html);
                $('#loading').addClass('hidden');
            }

            function get_comments_next_page() {
                var user_id = "<?php echo $this->session->userdata('userid'); ?>";
                var current_journal_id = $("#current_journal_id").val()
                var comments_current_page = parseInt($("#comments_current_page").val());
                var newpage = (comments_current_page + 1);
                newpage = adjust_comments_pagination(newpage);
                var comments_html = get_comments_html(user_id, current_journal_id, newpage);
                $("#comments_div").html(comments_html);
                $('#loading').addClass('hidden');
            }

            function adjust_comments_pagination(newpage) {
                $('#loading').removeClass('hidden');
                var no_of_pages = $("#comments_total_pages_num").text();
                if (no_of_pages != "") {
                    if (parseInt(newpage) >= parseInt(no_of_pages)) {
                        newpage = no_of_pages;
                        $("#comments_next_button").prop('disabled', true);
                    } else {
                        $("#comments_next_button").prop('disabled', false);
                    }
                } else {
                    newpage = 1;
                }
                if (parseInt(newpage) > 1) {
                    $("#comments_prev_button").prop('disabled', false);
                } else {
                    $("#comments_prev_button").prop('disabled', true);
                }
                $("#comments_current_page").val(newpage);
                $("#comments_desired_page_num").val(newpage);
                return newpage;
            }

        </script>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-119803421-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag() {
                dataLayer.push(arguments);
            }
            gtag('js', new Date());

            gtag('config', 'UA-119803421-1');
        </script>


    </body>

</html>
