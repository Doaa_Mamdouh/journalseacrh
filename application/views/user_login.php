<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<?php
$this->load->view('localization/lang');
$this->load->view('localization/txt');
$this->load->helper('url');
defineLocale();
defineStrings();
?>
<html lang="en" >
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <script type="text/javascript">
            function checkSubmit(e) {
                if (e && e.keyCode == 13) {
                    document.forms[0].submit();
                }
            }
            function checkValid() {
                var username = document.forms["login_form"]["username"].value;
                var password = document.forms["login_form"]["password"].value;
                if (username == "" || password == "") {
                    $("#wrong_msg").css("display", "block");
                    $("#wrong_msg").html("<?= INVALID_USERNAME_PASSWORD ?>")
                    return false;
                }
                return true;
            }
            function checkUsername() {
                $('#wrong_msg').css("display", "none");
                var username = document.forms["login_form"]["username"].value;
                if (username == "") {
                    $('#username_first').css("display", "block");
                    $('#wrong_username').css("display", "none");
                } else {
                    $('#username_first').css("display", "none");
                    var login_form = document.getElementById('login_form');
                    login_form.action = "<?= site_url('user_login/validateUsername') ?>";
                    login_form.submit();
                }
            }
        </script>
        <meta charset="utf-8" />
        <title><?php echo TITLE; ?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="Preview page of Metronic Admin Theme #3 for "
              name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link
            href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all"
            rel="stylesheet" type="text/css" />
        <link
            href="<?= base_url() ?>assets/global/plugins/font-awesome/css/font-awesome.min.css"
            rel="stylesheet" type="text/css" />
        <link
            href="<?= base_url() ?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css"
            rel="stylesheet" type="text/css" />
        <link
            href="<?= base_url() ?>assets/global/plugins/bootstrap/css/bootstrap.min.css"
            rel="stylesheet" type="text/css" />
        <link
            href="<?= base_url() ?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css"
            rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link
            href="<?= base_url() ?>assets/global/plugins/select2/css/select2.min.css"
            rel="stylesheet" type="text/css" />
        <link
            href="<?= base_url() ?>assets/global/plugins/select2/css/select2-bootstrap.min.css"
            rel="stylesheet" type="text/css" />
        <link href="<?= base_url() ?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url() ?>assets/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="<?= base_url() ?>assets/global/css/components.min.css"
              rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?= base_url() ?>assets/global/css/plugins.min.css"
              rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="<?= base_url() ?>assets/pages/css/login.min.css"
              rel="stylesheet" type="text/css" />
        <link href="<?= base_url() ?>assets/pages/css/search.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="<?= base_url() ?>assets/layouts/layout3/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url() ?>assets/layouts/layout3/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="<?= base_url() ?>assets/layouts/layout3/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="<?= base_url() ?>images/shortcut-icon.png" />

        <style type="text/css">
            .login {
                background: #eff3f8 !important;
            }

            .login-logo {
                margin-bottom: 40px;
                margin-top: 40px;
                max-width: 99% !important;
            }
        </style>

    </head>
    <!-- END HEAD -->

    <body class="page-container-bg-solid login">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">

                    <!-- BEGIN LOGIN -->
                    <div class="content">
                        <div style="text-align: center;">
                            <img src="<?= base_url() ?>images/Cubs_Journal_Logo.png" class="login-logo" >
                        </div>
                        <!-- BEGIN LOGIN FORM -->
                        <form role="form" method="post" id="login_form"
                              onsubmit="javascript:return checkValid();"
                              action="<?= site_url('user_login/login') ?>"
                              onKeyPress="return checkSubmit(event)">
                            <h3 class="form-title font-red"><?php echo SIGN_IN; ?></h3>
                            <div class="alert alert-danger display-hide">
                                <button class="close" data-close="alert"></button>
                                <span> <?php echo INSERT_USERNAME_PASSWORD; ?> </span>
                            </div>
                            <input id="token" type="hidden" name="token">
                            <div class="form-group">
                                <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                                <label class="control-label visible-ie8 visible-ie9"
                                       spellcheck="false"><?php echo USERNAME; ?></label> <input
                                       class="form-control form-control-solid placeholder-no-fix"
                                       id="username" type="text" autocomplete="off"
                                       placeholder="<?php echo USERNAME; ?>" name="user_name" />
                            </div>
                            <div class="form-group">
                                <label class="control-label visible-ie8 visible-ie9"><?php echo PASSWORD; ?></label>
                                <input class="form-control form-control-solid placeholder-no-fix"
                                       type="password" autocomplete="off"
                                       placeholder="<?php echo PASSWORD; ?>" name="password" />
                            </div>

                            <div id ="wrong_msg" class="form-group" <?php if (isset($code) && $code != "I000000") { ?>
                                     style="display: block; color: red;" <?php } else { ?>
                                     style="display: none; color:red;" <?php } ?>>
                                     <?php
                                     echo INVALID_USERNAME_PASSWORD;
                                     ?>
                            </div>
                            <div class="form-group" style="display: none; color: red;"
                                 id="username_first">
                                     <?php echo USERNAME_FIRST; ?>
                            </div>
                            <div class="form-group" style="display: none; color: red;"
                                 id="wrong_username">
                                     <?php echo WRONG_USERNAME; ?>
                            </div>

                            <div class="form-actions" style="text-align: center">
                                <button type="submit" class="btn red bold uppercase btn-block"><?php echo LOGIN; ?></button>
                                <br>
                                <div style="text-align: center; padding: 5px">
                                    <a class="bold font-red" onclick="javascript:checkUsername()" class="forget-password"> <?php echo CHANGE_PASSWORD; ?></a>
                                </div>
                            </div>
                        </form>

                        <!-- END LOGIN FORM -->
                    </div>
                </div>
            </div>
        </div>

        <!--[if lt IE 9]>
<script src="<?= base_url() ?>assets/global/plugins/respond.min.js"></script>
<script src="<?= base_url() ?>assets/global/plugins/excanvas.min.js"></script> 
<script src="<?= base_url() ?>assets/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="<?= base_url() ?>assets/global/plugins/jquery.min.js"
        type="text/javascript"></script>
        <script
            src="<?= base_url() ?>assets/global/plugins/bootstrap/js/bootstrap.min.js"
        type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/global/plugins/js.cookie.min.js"
        type="text/javascript"></script>
        <script
            src="<?= base_url() ?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"
        type="text/javascript"></script>
        <script
            src="<?= base_url() ?>assets/global/plugins/jquery.blockui.min.js"
        type="text/javascript"></script>
        <script
            src="<?= base_url() ?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"
        type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script
            src="<?= base_url() ?>assets/global/plugins/jquery-validation/js/jquery.validate.min.js"
        type="text/javascript"></script>
        <script
            src="<?= base_url() ?>assets/global/plugins/jquery-validation/js/additional-methods.min.js"
        type="text/javascript"></script>
        <script
            src="<?= base_url() ?>assets/global/plugins/select2/js/select2.full.min.js"
        type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/global/plugins/fancybox/source/jquery.fancybox.pack.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<?= base_url() ?>assets/global/scripts/app.min.js"
        type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?= base_url() ?>assets/pages/scripts/login.min.js"
        type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/pages/scripts/search.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="<?= base_url() ?>assets/layouts/layout3/scripts/layout.min.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/layouts/layout3/scripts/demo.min.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-119803421-1"></script>
        <script>
                                                window.dataLayer = window.dataLayer || [];
                                                function gtag() {
                                                    dataLayer.push(arguments);
                                                }
                                                gtag('js', new Date());

                                                gtag('config', 'UA-119803421-1');
        </script>

    </body>

</html>