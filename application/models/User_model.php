<?php

class User_model extends CI_Model {

    function getUsers($page_size, $page_number, $search_text) {
        $this->db->trans_begin();
        $offset = ($page_number * $page_size) - $page_size;
        $whereConditions = "";
        
        if ($search_text != "") {
            $whereConditions = " and (name like '%" . $search_text . "%' or user_name like '%".$search_text."%' or id_number like '%".$search_text."%')";
        }
        $query = $this->db->query("SELECT user.`id`,`id_number`,`name`, `user_name`
                    FROM user where deleted = 'N' " .$whereConditions." order by id "
                . " LIMIT " . $page_size . " OFFSET " . $offset);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return "E000100";
        } else {
            $users = $query->result_array();
        }
        $this->db->trans_complete();
        return $users;
    }

    function countUsers($search_text) {
        $this->db->trans_begin();
        if ($search_text != "") {
            $whereConditions = " and (name like '%" . $search_text . "%' or user_name like '%".$search_text."%')";
        }
        $query = $this->db->query("SELECT user.`id`
                    FROM user where deleted = 'N' ".$whereConditions);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return "E000100";
        } else {
            $count = $query->num_rows();
        }
        $this->db->trans_complete();
        return $count;
    }

    function getUser($id) {
        $this->db->trans_begin();
        $query = $this->db->query("SELECT `id`,`id_number`,`name`, `user_name`, `password`
                    FROM user
                    where user.id = " . $id);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return "E000100";
        } else {
            $user = $query->result_array()[0];
        }
        $this->db->trans_complete();
        return $user;
    }

    function insertUser($data) {
        $this->db->insert("user", $data);
        return $this->db->insert_id();
    }

    function updateUser($data) {
        $where = "id = " . $data["id"];
        $this->db->update("user", $data, $where);
        return $this->db->affected_Rows();
    }

    function deleteUser($id) {
        $where = "id = " . $id;
        //$this->db->delete("user",$where);
        $data = array();
        $data["deleted"] = 'Y';
        $this->db->update("user", $data, $where);
        return $this->db->affected_Rows();
    }

    function checkUserRepeat($user_name, $rec_id = 0) {
        $this->db->trans_begin();
        $whereConditions = "WHERE user_name = '" . $user_name . "'";
        if ($rec_id > 0) {
            $whereConditions = $whereConditions . " and id != " . $rec_id;
        }
        $query_str = "SELECT id
                    FROM user " . $whereConditions;

        $query = $this->db->query($query_str);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return "E000100";
        } else if (count($query->result_array()) > 0) {
            return "E000101";
        }
        $this->db->trans_complete();
        return false;
    }

    function checkUserRepeatUniversityId($id_number, $rec_id = 0) {
        $this->db->trans_begin();
        $whereConditions = "WHERE id_number = '" . $id_number . "'";
        if ($rec_id > 0) {
            $whereConditions = $whereConditions . " and id != " . $rec_id;
        }
        $query_str = "SELECT id
                    FROM user " . $whereConditions;

        $query = $this->db->query($query_str);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return "E000100";
        } else if (count($query->result_array()) > 0) {
            return "E000102";
        }
        $this->db->trans_complete();
        return false;
    }

}
