<?php

class User_Favorites_model extends CI_Model
{

    function insertUserFavoriteJournal($data){
        $this->db->trans_begin();
        $this->db->insert("user_favorites", $data);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return "E000100";
        }
        $this->db->trans_complete();
        return $this->db->insert_id();
    }
    
    function deleteUserFavoriteJournal($data){
        $this->db->trans_begin();
        $where = "user_id = " . $data['user_id']." AND journal_id = ". $data['journal_id'];
        $this->db->delete("user_favorites", $where);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return "E000100";
        }
        $this->db->trans_complete();
        return $this->db->affected_Rows();
    }
    
 
}