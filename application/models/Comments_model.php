<?php

class Comments_model extends CI_Model
{

    function getComment($comment_id)
    {
        $this->db->trans_begin();
        $query = $this->db->query("SELECT `id`, `journal_id`, `user_id`, `description`, `status`, `admin_id`
                        FROM comment
                        WHERE `id` = '" . $comment_id . "'");
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return "E000100";
        } else {
            $records = $query->result_array();
        }
        $this->db->trans_complete();
        return $records;
    }

    function addJournalComment($data)
    {
        $this->db->trans_begin();
        $table_name = "comment";
        $this->db->insert($table_name, $data);
        $comment_id = $this->db->insert_id();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return "E000100";
        }
        $this->db->trans_complete();
        return $comment_id;
    }

    function getJournalCommentsList($user_id, $journal_id, $page_size, $page_num)
    {
        $this->db->trans_begin();
        if ($user_id == '' || $user_id == null || $user_id == 'null') {
            return array();
        }
        if (!is_numeric($page_num) || !is_numeric($page_size)) {
            return "E000100";
        }
        $offset = ($page_num * $page_size) - $page_size;
        $query = $this->db->query("SELECT `comment`.`id` as comment_id, `journal_id`, `user_id`, `description`, `status`, `comment_date`, `admin_id`, `user`.`user_name`
                        FROM comment
                        left outer JOIN user on user.id = comment.user_id
                        WHERE journal_id = " . $journal_id . "  AND ( (status = 'A' ) OR (status = 'P' AND comment.user_id = " . $user_id . ") )
                        ORDER BY `comment`.`comment_date` DESC LIMIT " . $page_size . " OFFSET " . $offset);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return "E000100";
        } else {
            $records = $query->result_array();
        }
        $this->db->trans_complete();
        return $records;
    }

    function getPendingCommentsList($page_size, $page_num)
    {
        $this->db->trans_begin();
        if (!is_numeric($page_num) || !is_numeric($page_size)) {
            return "E000100";
        }
        $offset = ($page_num * $page_size) - $page_size;
        $query = $this->db->query("SELECT `comment`.`id` as comment_id, `journal_id`, `user_id`, `description`, `status`, `comment_date`, `admin_id`, `user`.`user_name`, `journal`.`title` as journal_title
                        FROM comment 
                        left outer JOIN user on user.id = comment.user_id
                        left outer JOIN journal on journal.id = comment.journal_id
                        WHERE status = 'P' ORDER BY `comment`.`comment_date` DESC LIMIT " . $page_size . " OFFSET " . $offset);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return "E000100";
        } else {
            $records = $query->result_array();
        }
        $this->db->trans_complete();
        return $records;
    }

    function countPendingComments() {
        $this->db->trans_begin();
        $query = $this->db->query("SELECT id FROM comment WHERE status = 'P' ");
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return "E000100";
        } else {
            $count = $query->num_rows();
        }
        $this->db->trans_complete();
        return $count;
    }
    
    function countComments($user_id, $journal_id){
        $this->db->trans_begin();
        $query = $this->db->query("SELECT id FROM comment  WHERE journal_id = " . $journal_id . "  AND ( (status = 'A' ) OR (status = 'P' AND comment.user_id = " . $user_id . ") )" );
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return "E000100";
        } else {
            $count = $query->num_rows();
        }
        $this->db->trans_complete();
        return $count;
    }
    
    function executeCommentAction($data)
    {
        $this->db->trans_begin();
        $table_name = "comment";
        $where = "id ='" . $data['id'] . "'";
        $this->db->update($table_name, $data, $where);
        $affected_Rows = $this->db->affected_Rows();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return "E000100";
        }
        $this->db->trans_complete();
        return $affected_Rows;
    }
}