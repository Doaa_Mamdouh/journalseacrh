<?php

class Journal_model extends CI_Model {

    function getJournals($search_criteria, $page_size, $page_number, $all_data = false, $fav_only = false) {
        $this->db->trans_begin();
        if (!is_numeric($page_number) || !is_numeric($page_size)) {
            return "E000100";
        }
        $user_id = "";
        $fav_query = "";
        $fav_col = "";
        $fav_join_type = "LEFT OUTER JOIN";
        $offset = ($page_number * $page_size) - $page_size;
        $whereConditions = "order by abs_rank desc,id ";
        if (!$all_data) {
            $whereConditions = "WHERE " . self::getWhereConditions($search_criteria);
        }
        if ($fav_only) {
            $fav_join_type = "INNER JOIN";
        }
        if (isset($search_criteria ['user_id'])) {
            $user_id = $search_criteria ['user_id'];
            $fav_query = $fav_join_type . " 
                    
                        (
                           SELECT distinct(user_favorites.journal_id) , user_favorites.id as user_favorites_id
                           FROM user_favorites
                           where  user_favorites.user_id = '" . $user_id . "' 
                        ) uf ON journal.`id` = uf.journal_id
                     ";
            $fav_col = ", user_favorites_id";
        }
        $query = $this->db->query("SELECT journal.`id`,`title`, `field`, `abs_rank`, country.name as domicile_name, `impact_factor`, `frequency`, `financial_times_ranking`, `articles_num`" . $fav_col . "
                    FROM journal
                    left outer JOIN country on country.id = domicile " . $fav_query
                . $whereConditions
                . " LIMIT " . $page_size . " OFFSET " . $offset);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return "E000100";
        } else {
            $records = $query->result_array();
        }
        $this->db->trans_complete();
        return $records;
    }

    function getJournal($journal_id, $user_id = "") {
        $this->db->trans_begin();
        $journal = null;
        $query_str = "SELECT journal.`id`,`title`, `field`, `abs_rank`, country.name as domicile_name, `impact_factor`, `frequency`, `financial_times_ranking`
                        , `issn`, `abs_2015`, `intr`, `sjr`, `articles_num`, `staff_views_advice`, `ajg_2018`, `ajg_2015`
                        , `abs_2010`, `abs_2009`, `jcr_rank`, `sjr_rank`, `snip_rank`, `ipp_rank`,`last_update_date`,`url`, user_favorites_id
                    FROM journal
                    left outer JOIN country on country.id = domicile
                    LEFT OUTER JOIN 
                        (
                           SELECT distinct(user_favorites.journal_id) , user_favorites.id as user_favorites_id
                           FROM user_favorites
                           where  user_favorites.user_id = '" . $user_id . "' 
                        ) uf ON journal.`id` = uf.journal_id
                    WHERE journal.id = " . $journal_id;
        $query = $this->db->query($query_str);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return "E000100";
        } else {
            $journal = $query->result_array()[0];
        }
        $this->db->trans_complete();
        return $journal;
    }

    function checkJournalRepeat($journal_title, $journal_field, $rec_id = 0, $importing = false) {
        $this->db->trans_begin();
        $whereConditions = 'WHERE journal.title = "' . $journal_title . '" and field = "' . $journal_field . '"';
        if ($rec_id > 0) {
            $whereConditions = $whereConditions . ' and id != ' . $rec_id;
        }
        $query_str = "SELECT id
                    FROM journal " . $whereConditions;

        $query = $this->db->query($query_str);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return "E000100";
        } else if (count($query->result_array()) > 0) {
            if (!$importing) {
                return "E000101"; // for admin panel update or insert with values already there
            } else {
                return $query->result_array()[0]["id"]; // get id of updated record
            }
        }
        $this->db->trans_complete();
        return false;
    }

    function countJournals($search_criteria, $all_data = false, $fav_only = false) {
        $user_id = "";
        if (isset($search_criteria ['user_id'])) {
            $user_id = $search_criteria ['user_id'];
        }
        if (!$all_data) {
            $whereConditions = "WHERE " . self::getWhereConditions($search_criteria);
        }
        if ($fav_only) {
            $whereConditions = "inner join (
                           SELECT distinct(user_favorites.journal_id) , user_favorites.id as user_favorites_id
                           FROM user_favorites
                           where  user_favorites.user_id = '" . $user_id . "' 
                        ) uf ON journal.`id` = uf.journal_id";
        }
        $this->db->trans_begin();
        $query = $this->db->query("SELECT id
                    FROM journal
                    " . $whereConditions);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return "E000100";
        } else {
            $count = $query->num_rows();
        }
        $this->db->trans_complete();
        return $count;
    }

    function insertJournal($data) {
        $this->db->trans_begin();
        $data["last_update_date"] = date("Y-m-d");
        $this->db->insert("journal", $data);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return "E000100";
        }
        $this->db->trans_complete();
        return $this->db->insert_id();
    }

    function updateJournal($data) {
        $this->db->trans_begin();
        $data["last_update_date"] = date("Y-m-d");
        $where = "id = " . $data["id"];
        $this->db->update("journal", $data, $where);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return "E000100";
        }
        $this->db->trans_complete();
        return $this->db->affected_Rows();
    }

    function deleteJournal($id) {
        $this->db->trans_begin();
        $where = "id = " . $id;
        $this->db->delete("journal", $where);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return "E000100";
        }
        $this->db->trans_complete();
        return $this->db->affected_Rows();
    }

    function countJournalsPerField() {
        $this->db->trans_begin();
        $offset = ($page_number * $page_size) - $page_size;
        $query = $this->db->query("SELECT FIELD as journals_field, COUNT(*)as journals_count FROM journal GROUP by FIELD");
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return "E000100";
        } else {
            $fields_count = $query->result_array();
        }
        $this->db->trans_complete();
        return $fields_count;
    }

    function getDomicileList($search_criteria) {
        $this->db->trans_begin();
        $whereConditions = "";
        if ($search_criteria != "") {
            $search_criteria["domicile_ids"] = "null";
            $whereConditions = self::getWhereConditions($search_criteria);
            if ($whereConditions != "") {
                $whereConditions = "and country.`id` in (select domicile from journal where " . $whereConditions . ")";
            }
        }
        $query = $this->db->query("SELECT country.`id`, country.`name` FROM country where name is not null " . $whereConditions);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return "E000100";
        } else {
            $records = $query->result_array();
        }
        $this->db->trans_complete();
        return $records;
    }

    function getFieldsList($search_criteria) {
        $this->db->trans_begin();
        $whereConditions = "";
        if ($search_criteria != "") {
            $search_criteria["field"] = "null";
            $whereConditions = self::getWhereConditions($search_criteria);
        }
        if ($whereConditions != "") {
            $whereConditions = " and " . $whereConditions;
        }

        $query = $this->db->query("SELECT DISTINCT(journal.`field`) as name FROM journal where field is not null and field <> '' " . $whereConditions);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return "E000100";
        } else {
            $records = $query->result_array();
        }
        $this->db->trans_complete();
        return $records;
    }

    function getABSRankList($search_criteria) {
        $this->db->trans_begin();
        $whereConditions = "";
        if ($search_criteria != "") {
            $search_criteria["abs_rank"] = "null";
            $whereConditions = self::getWhereConditions($search_criteria);
        }
        if ($whereConditions != "") {
            // remove search title order
            $pos = strpos($whereConditions, "order by");
            if ($pos !== false) {
                $order_string = substr($whereConditions, $pos);
                $whereConditions = str_replace($order_string, "", $whereConditions);
            }
            $whereConditions = " and " . $whereConditions;
        }
        $query = $this->db->query("SELECT DISTINCT(journal.`abs_rank`) as name FROM journal where abs_rank is not null and field <> ''" . $whereConditions . " ORDER BY `journal`.`abs_rank` ASC ");
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return "E000100";
        } else {
            $records = $query->result_array();
        }
        $this->db->trans_complete();
        return $records;
    }

    function getArticlesNumberBoundaries($search_criteria) {
        $this->db->trans_begin();
        $whereConditions = "";
        if ($search_criteria != "") {
            $whereConditions = self::getWhereConditions($search_criteria);
        }
        if ($whereConditions != "") {
            $whereConditions = " where " . $whereConditions;
        }
        $query = $this->db->query("SELECT min(journal.articles_num) as min_articles_number, max(journal.articles_num) as max_articles_number FROM journal " . $whereConditions);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return "E000100";
        } else {
            $records = $query->result_array();
        }
        $this->db->trans_complete();
        return $records;
    }

    /*
     * SELECT * FROM `journal`
      WHERE (title LIKE '%Management%information%' or (title LIKE '%information%' and title LIKE '%Management%' AND title NOT LIKE '%Management%information%'))
     *  and title RLIKE '[[:<:]]Management[[:>:]]' and title RLIKE '[[:<:]]information[[:>:]]' 
      order by case
      when title LIKE '%Management%information%' then 1
      when title LIKE '%information%' and title LIKE '%Management%' AND title NOT LIKE '%Management%information%'  then 2
      else 3
      end, abs_rank DESC
     */

    private function titleFieldSearch($search_criteria, $search_criteria2, $search_field) {
        $whereSearchCriteria1 = self::whereConditionSearchText($search_criteria, $search_field);
        $whereSearchCriteria2 = self::whereConditionSearchText($search_criteria2, $search_field);
        $whereConditions = "((" . $whereSearchCriteria1[0] . ") or (" . $whereSearchCriteria2[0] . "))" .
                "order by case 
            when " . $whereSearchCriteria1[1] . " then 1 when " . $whereSearchCriteria2[1] . " then 2 when " . $whereSearchCriteria1[2] . " then 3 when " . $whereSearchCriteria2[2] . " then 4 else 5 end ,abs_rank DESC,id";
        return $whereConditions;
    }

    private function whereConditionSearchText($search_criteria, $search_field) {
        $search_parts = explode(" ", $search_criteria);
        $whereConditions = "";
        $whereConditions1 = $search_field . " like '%";
        $whereConditions2 = "(";
        $whereConditions3 = "";
        $itr = 1;
        foreach ($search_parts as $search_string) {
            $whereConditions1 = $whereConditions1 . $search_string . "%";
            $whereConditions2 = $whereConditions2 . $search_field . " LIKE '%" . $search_string . "%'";
            if ($search_string != '&') {
                $whereConditions3 = $whereConditions3 . $search_field . " RLIKE '[[:<:]]" . $search_string . "[[:>:]]' ";
            }
            if ($itr < count($search_parts)) {
                $whereConditions2 = $whereConditions2 . " and ";
                if ($search_string != '&') {
                    $whereConditions3 = $whereConditions3 . " and ";
                }
                $itr = $itr + 1;
            } else {
                $whereConditions1 = $whereConditions1 . "'";
                $whereConditions2 = $whereConditions2 . " and " . str_replace("like", "not like", $whereConditions1) . ")";
            }
        }
        $whereConditions = "((" . $whereConditions1 . " or " . $whereConditions2 . ") and " . $whereConditions3 . ")";
        $whereCondList = array();
        $whereCondList[0] = $whereConditions;
        $whereCondList[1] = $whereConditions1;
        $whereCondList[2] = $whereConditions2;
        $whereCondList[3] = $whereConditions3;
        return $whereCondList;
    }

    private function appendCondition($whereConditions, $condition) {
        if ($whereConditions == "") {
            $whereConditions = $condition;
        } else {
            $whereConditions = $whereConditions . " and " . $condition;
        }
        return $whereConditions;
    }

    private function getWhereConditions($search_criteria) {
        $whereConditions = "";
        if (!isset($search_criteria["field"])) {
            $whereConditions = self::titleFieldSearch($search_criteria["title"], $search_criteria["title2"], "title");
        } else {
            if ($search_criteria["field"] != "null") {
                $fields_arr = explode("_", $search_criteria["field"]);
                $fields = "";
                foreach ($fields_arr as $field) {
                    $fields = $fields . "'" . $field . "'";
                    if (array_search($field, $fields_arr) < count($fields_arr) - 1) {
                        $fields = $fields . ",";
                    }
                }
                $whereConditions = "field in (" . $fields . ")";
            }if ($search_criteria["abs_rank"] != "null") {
                $whereConditions = self::appendCondition($whereConditions, "abs_rank in (" . $search_criteria["abs_rank"] . ")");
            }if ($search_criteria["domicile_ids"] != "null") {
                $whereConditions = self::appendCondition($whereConditions, "domicile in (" . $search_criteria["domicile_ids"] . ")");
            }
            $whereConditions = self::appendCondition($whereConditions, "(articles_num BETWEEN " . $search_criteria["articles_number_from"] . " AND " . $search_criteria["articles_number_to"] . ") ");
            if ($search_criteria["title"] != "null") {
                $whereConditions = self::appendCondition($whereConditions, self::titleFieldSearch($search_criteria["title"], $search_criteria["title2"], "title"));
            } else {
                $whereConditions = $whereConditions . " order by abs_rank DESC,id";
            }
        }
        return $whereConditions;
    }

    function getFrequencyBoundaries() {
        $this->db->trans_begin();
        $query = $this->db->query("SELECT min(journal.frequency) as min_frequency, max(journal.frequency) as max_frequency FROM journal ");
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return "E000100";
        } else {
            $records = $query->result_array();
        }
        $this->db->trans_complete();
        return $records;
    }

    function getImpactFactorsBoundaries() {
        $this->db->trans_begin();
        $query = $this->db->query("SELECT min(journal.impact_factor) as min_impact_factor, max(journal.impact_factor) as max_impact_factor FROM journal ");
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return "E000100";
        } else {
            $records = $query->result_array();
        }
        $this->db->trans_complete();
        return $records;
    }

}
