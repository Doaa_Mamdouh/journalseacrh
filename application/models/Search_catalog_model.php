<?php
class Search_catalog_model extends CI_Model {
    function getSearchCatalog (){
        $query = $this->db->query("SELECT `key` , `value`
                    FROM search_catalog where value != ' '");
        $records = $query->result_array ();
        $catalog_hash = array();
        foreach ($records as $record){
            $catalog_hash [strtoupper($record["key"])] = $record["value"];
        }
        return $catalog_hash;
    }
    function getPunctuations (){
        $query = $this->db->query("SELECT `key` , `value`
                    FROM search_catalog where value = ' '");
        $records = $query->result_array ();
        return $records;
    }
}