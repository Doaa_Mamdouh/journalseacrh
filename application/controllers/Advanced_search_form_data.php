<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (! defined('BASEPATH'))
    exit('No direct script access allowed');

class Advanced_search_form_data extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model("journal_model");
    }

    function index()
    {
        self::getDomicileList();
    }

    function getDomicileList($search_creiteria = "")
    {
        try {
            $result = $this->journal_model->getDomicileList($search_creiteria);
            if (gettype($result) == "string") {
                $data["code"] = $result; // DB Error
                echo json_encode($data);
                return;
            } else {
                $domicileList = $result;
            }
            $domicileList_to_send = array();
            foreach ($domicileList as $domicile) {
                $domicileList_to_send[$domicile["id"]]["name"] = $domicile["name"];
            }
            $count = count($domicileList_to_send);
            $data["domicileList"] = $domicileList_to_send;
            $data["domicileListCount"] = $count;
            $data["code"] = "I000000"; // Successful
            echo json_encode($data);
        } catch (Exception $e) {
            $data["code"] = "E999999"; // Unhandled Error
            echo json_encode($data);
        }
    }

    function getFieldsList($search_creiteria = "")
    {
        try {
            $result = $this->journal_model->getFieldsList($search_creiteria);
            if (gettype($result) == "string") {
                $data["code"] = $result; // DB Error
                echo json_encode($data);
                return;
            } else {
                $fieldsList = $result;
            }
            $fieldsList_to_send = array();
            foreach ($fieldsList as $field) {
                $fieldsList_to_send[$field["name"]]["name"] = $field["name"];
            }
            $count = count($fieldsList_to_send);
            $data["fieldsList"] = $fieldsList_to_send;
            $data["fieldsListCount"] = $count;
            $data["code"] = "I000000"; // Successful
            echo json_encode($data);
        } catch (Exception $e) {
            $data["code"] = "E999999"; // Unhandled Error
            echo json_encode($data);
        }
    }
    
    function getABSRankList($search_creiteria = "")
    {
        try {
            $result = $this->journal_model->getABSRankList($search_creiteria);
            if (gettype($result) == "string") {
                $data["code"] = $result; // DB Error
                echo json_encode($data);
                return;
            } else {
                $ABSRankList = $result;
            }
            $ABSRankList_to_send = array();
            foreach ($ABSRankList as $ABSRank) {
                $ABSRankList_to_send[$ABSRank["name"]]["name"] = $ABSRank["name"];
            }
            $count = count($ABSRankList_to_send);
            $data["ABSRankList"] = $ABSRankList_to_send;
            $data["ABSRankListCount"] = $count;
            $data["code"] = "I000000"; // Successful
            echo json_encode($data);
        } catch (Exception $e) {
            $data["code"] = "E999999"; // Unhandled Error
            echo json_encode($data);
        }
    }

    function getFrequencyBoundaries()
    {
        try {
            $result = $this->journal_model->getFrequencyBoundaries();
            if (gettype($result) == "string") {
                $data["code"] = $result; // DB Error
                echo json_encode($data);
                return;
            } else {
                $frequencyBoundaries = $result;
            }
            $frequencyBoundaries_to_send = array();
            foreach ($frequencyBoundaries as $frequencyBoundary) {
                $frequencyBoundaries_to_send["min_frequency"] = $frequencyBoundary["min_frequency"];
                $frequencyBoundaries_to_send["max_frequency"] = $frequencyBoundary["max_frequency"];
            }
            $data["frequencyBoundaries"] = $frequencyBoundaries_to_send;
            $data["code"] = "I000000"; // Successful
            echo json_encode($data);
        } catch (Exception $e) {
            $data["code"] = "E999999"; // Unhandled Error
            echo json_encode($data);
        }
    }
    
    function getImpactFactorsBoundaries()
    {
        try {
            $result = $this->journal_model->getImpactFactorsBoundaries();
            if (gettype($result) == "string") {
                $data["code"] = $result; // DB Error
                echo json_encode($data);
                return;
            } else {
                $impactFactorsBoundaries = $result;
            }
            $impactFactorsBoundaries_to_send = array();
            foreach ($impactFactorsBoundaries as $impactFactorsBoundary) {
                $impactFactorsBoundaries_to_send["min_impact_factor"] = $impactFactorsBoundary["min_impact_factor"];
                $impactFactorsBoundaries_to_send["max_impact_factor"] = $impactFactorsBoundary["max_impact_factor"];
            }
            $data["impactFactorsBoundaries"] = $impactFactorsBoundaries_to_send;
            $data["code"] = "I000000"; // Successful
            echo json_encode($data);
        } catch (Exception $e) {
            $data["code"] = "E999999"; // Unhandled Error
            echo json_encode($data);
        }
    }
    
    function getArticlesNumberBoundaries($search_creiteria = "")
    {
        try {
            $result = $this->journal_model->getArticlesNumberBoundaries($search_creiteria);
            if (gettype($result) == "string") {
                $data["code"] = $result; // DB Error
                echo json_encode($data);
                return;
            } else {
                $ArticlesNumberBoundaries = $result;
            }
            $ArticlesNumberBoundaries_to_send = array();
            foreach ($ArticlesNumberBoundaries as $ArticlesNumberBoundary) {
                $ArticlesNumberBoundaries_to_send["min_articles_number"] = $ArticlesNumberBoundary["min_articles_number"];
                $ArticlesNumberBoundaries_to_send["max_articles_number"] = $ArticlesNumberBoundary["max_articles_number"];
            }
            $data["ArticlesNumberBoundaries"] = $ArticlesNumberBoundaries_to_send;
            $data["code"] = "I000000"; // Successful
            echo json_encode($data);
        } catch (Exception $e) {
            $data["code"] = "E999999"; // Unhandled Error
            echo json_encode($data);
        }
    }
    
}