<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (! defined('BASEPATH'))
    exit('No direct script access allowed');

class Comments_controller extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model("comments_model");
    }

    public function index()
    {
        addJournalComment();
    }

    function addJournalComment()
    {
        try {
            if (extract($_POST)) {
                $comment_data = array();
                $comment_data['journal_id'] = $this->input->post('journal_id');
                $comment_data['user_id'] = $this->input->post('user_id');
                $comment_data['description'] = $this->input->post('description');
                $comment_data['status'] = 'P';
                $result = $this->comments_model->addJournalComment($comment_data);
                $data = array();
                if (strpos($result, "E") !== false) {
                    $data["code"] = $result; // DB Error
                } else {
                    $data["comment_id"] = $result;
                    $data["status"] = $comment_data['status'];
                    $data["code"] = "I000000";
                }
                echo json_encode($data);
            }
        } catch (Exception $e) {
            $data["code"] = "E999999"; // Unhandled Error
            echo json_encode($data);
        }
    }

    function getJournalCommentsList($user_id, $journal_id, $page_size = 20, $page_num = 1)
    {
        try {
            $result = $this->comments_model->getJournalCommentsList($user_id, $journal_id, $page_size, $page_num);
            if (gettype($result) == "string") {
                $data["code"] = $result; // DB Error
                echo json_encode($data);
                return;
            } else {
                $commentsList = $result;
                $count = $this->comments_model->countComments($user_id, $journal_id);
                
                if (strpos($count, "E") !== false) {
                    $data["code"] = $count; //DB Error
                    echo json_encode($data);
                    return;
                }
                
                $commentsList_to_send = array();
                $i = 0;
                foreach ($commentsList as $comment) {
                    $commentsList_to_send[$i]["comment_id"] = $comment["comment_id"];
                    $commentsList_to_send[$i]["user_name"] = $comment["user_name"];
                    $commentsList_to_send[$i]["description"] = $comment["description"];
                    $commentsList_to_send[$i]["status"] = $comment["status"];
                    $commentsList_to_send[$i]["comment_date"] = $comment["comment_date"];
                    $i++;
                }
                $data["commentList"] = $commentsList_to_send;
                $data["total_count"] = $count;
                $data["code"] = "I000000"; // Successful
                echo json_encode($data);
            }
        } catch (Exception $e) {
            $data["code"] = "E999999"; // Unhandled Error
            echo json_encode($data);
        }
    }


    function executeCommentAction()
    {
        try {
            if (extract($_POST)) {
                $comment_data = array();
                $comment_data['admin_id'] = $this->input->post('admin_id');
                $comment_data['id'] = $this->input->post('comment_id');
                $comment_data['status'] = $this->input->post('action');
                $result = $this->comments_model->executeCommentAction($comment_data);
                $data = array();
                if (strpos($result, "E") !== false) {
                    $data["code"] = $result; // DB Error
                } else {
                    $data["code"] = "I000000";
                }
                echo json_encode($data);
            }
        } catch (Exception $e) {
            $data["code"] = "E999999"; // Unhandled Error
            echo json_encode($data);
        }
    }
}

