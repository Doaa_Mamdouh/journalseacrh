<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (! defined ( 'BASEPATH' ))
    exit ( 'No direct script access allowed' );

class Journals_meta_data extends CI_Controller {
    function __construct() {
		parent::__construct ();
                $this->load->model ( "journal_model" );
                $this->load->model ( "Search_catalog_model" );
	}
	 function index()
        {
                self::getJournalsChart ();
        }
        
        function getJournalsChart(){
            try{
                $result = $this->journal_model->countJournalsPerField ();
                if (gettype($result) == "string"){
                    $data["code"] = $result; //DB Error
                    echo json_encode ( $data );
                    return;
                }else {
                    $fields_count = $result;
                    $data["fields_count"] = $fields_count;
                    $data["code"] = "I000000"; //Successful
                    echo json_encode ( $data );
                }
            } catch (Exception $e){
                $data["code"] = "E999999"; //Unhandled Error
                echo json_encode ( $data );
            }
        }
}