<?php

defined('BASEPATH') OR exit('No direct script access allowed');

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Import_journals extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("journal_model");
        $this->load->model("country_model");
    }

    function index() {
        self::excel_import();
    }

    function excel_import() {
        //$inputFileName = 'D:/Aurora/journalsearch-Files/Journal Ranking_Final_List_Update_July2019.xlsx';
        $inputFileName = '/home/cubs/public_html/journalsearch/application/controllers/Journal_Ranking_Final_List_Update_July2019.xlsx';
        include 'Classes/PHPExcel/IOFactory.php';
        $objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
        $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
        $arrayCount = count($allDataInSheet); // Here get total count of row in that Excel sheet
        // looping on Excel sheet data
        for ($i = 2; $i <= $arrayCount; $i ++) {
            $journal_data = array();
            $journal_data["issn"] = $allDataInSheet [$i] ["A"];
            $journal_data["title"] = $allDataInSheet [$i] ["B"];
            $journal_data["abs_rank"] = $allDataInSheet [$i] ["C"];
            if ($journal_data["abs_rank"] == "") {
                $journal_data["abs_rank"] = 0;
            }
            /*$journal_data["abs_2015"] = $allDataInSheet [$i] ["D"];
            if ($journal_data["abs_2015"] == "") {
                $journal_data["abs_2015"] = 0;
            }*/
            $journal_data["intr"] = $allDataInSheet [$i] ["D"];
            if ($journal_data["intr"] == "") {
                $journal_data["intr"] = 0;
            }
            $journal_data["sjr"] = $allDataInSheet [$i] ["I"];
            if ($journal_data["sjr"] == "") {
                $journal_data["sjr"] = 0;
            }
            $journal_data["field"] = $allDataInSheet [$i] ["E"];
            
            if ($allDataInSheet [$i] ["F"] != null || $allDataInSheet [$i] ["F"] != "") {
                $country = $this->country_model->getCountry($allDataInSheet [$i] ["F"]);
                if (count($country) > 0) {
                    $journal_data["domicile"] = $country[0]["id"];
                } else {
                    $country = array();
                    $country["name"] = $allDataInSheet [$i] ["F"];
                    $country_id = $this->country_model->insertCountry($country);
                    $journal_data["domicile"] = $country_id;
                }
            }
            /*$journal_data["frequency"] = $allDataInSheet [$i] ["G"];
            if ($journal_data["frequency"] == "") {
                $journal_data["frequency"] = 0;
            }*/
            $journal_data["articles_num"] = $allDataInSheet [$i] ["G"];
            if ($journal_data["articles_num"] == "") {
                $journal_data["articles_num"] = 0;
            }
            //$journal_data["staff_views_advice"] = $allDataInSheet [$i] ["H"];
            $journal_data["url"] = $allDataInSheet [$i] ["H"];
           /* $journal_data["impact_factor"] = $allDataInSheet [$i] ["I"];
            if ($journal_data["impact_factor"] == "") {
                $journal_data["impact_factor"] = 0;
            }*/
            $journal_data["last_update_date"] = date("Y-m-d");
           /* if ($allDataInSheet [$i] ["O"] == "Y") {
                $journal_data["financial_times_ranking"] = 1;
            } else {
                $journal_data["financial_times_ranking"] = 0;
            }
            

            $journal_data["ajg_2018"] = $allDataInSheet [$i] ["W"];
            if ($journal_data["ajg_2018"] == "") {
                $journal_data["ajg_2018"] = 0;
            }
            $journal_data["ajg_2015"] = $allDataInSheet [$i] ["X"];
            if ($journal_data["ajg_2015"] == "") {
                $journal_data["ajg_2015"] = 0;
            }
            $journal_data["abs_2010"] = $allDataInSheet [$i] ["Y"];
            if ($journal_data["abs_2010"] == "") {
                $journal_data["abs_2010"] = 0;
            }
            $journal_data["abs_2009"] = $allDataInSheet [$i] ["Z"];
            if ($journal_data["abs_2009"] == "") {
                $journal_data["abs_2009"] = 0;
            }
            $journal_data["jcr_rank"] = $allDataInSheet [$i] ["AA"];
            if ($journal_data["jcr_rank"] == "") {
                $journal_data["jcr_rank"] = 0;
            }
            $journal_data["sjr_rank"] = $allDataInSheet [$i] ["AB"];
            if ($journal_data["sjr_rank"] == "") {
                $journal_data["sjr_rank"] = 0;
            }
            $journal_data["snip_rank"] = $allDataInSheet [$i] ["AC"];
            if ($journal_data["snip_rank"] == "") {
                $journal_data["snip_rank"] = 0;
            }
            $journal_data["ipp_rank"] = $allDataInSheet [$i] ["AD"];
            if ($journal_data["ipp_rank"] == "") {
                $journal_data["ipp_rank"] = 0;
            }*/
           /* $id = $this->journal_model->checkJournalRepeat($journal_data["title"], $journal_data["field"],0,true);
            if ($id == false) {*/
                $this->journal_model->insertJournal($journal_data);
           /* } else {
                $journal_data["id"] = $id;
                $this->journal_model->updatejournal($journal_data);
            }*/
        }
    }
}
