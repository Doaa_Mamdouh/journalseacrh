<?php

defined('BASEPATH') OR exit('No direct script access allowed');

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User_login extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->model("common");
        $this->load->library('encrypt');
    }

    function index() {
        $data ['msg'] = "";
        if ($this->session->userdata('userid') != '') {
            $user_id = $this->session->userdata('userid');
            redirect('user_panel', 'refresh');
        } else {
            $data ['user_name'] = '';
            $this->load->view('user_login', $data);
        }
    }

    function login() {
        if (extract($_POST)) {

            $username = $this->common->mysql_safe_string($this->input->post('user_name'));

            $password = $this->common->mysql_safe_string($this->input->post('password'));

            if ($username != "" && $password != "") {
                $data = self::checkUserValidity($username, $password);
                if ($data["code"] == "I000000") {
                    if ($data['first_login'] == 'Y') {
                        $this->load->view('change_password', $data);
                    } else {
                        $user_id = $data ['id'];
                        $userdata = array(
                            'userid' => $data ['id'],
                            'user_name' => $data ['user_name'],
                            'id_number' => $data['id_number'],
                            'user_logged' => TRUE
                        );
                        $this->session->set_userdata($userdata);
                        redirect('user_login', 'refresh');
                    }
                } else {
                    $this->load->view('user_login', $data);
                }
            } else {
                if ($username == "") {
                    $data ['code'] = "E000100";
                } else {
                    $data ['code'] = "E000101";
                }
                $this->load->view('user_login', $data); // Executes if username is entered incorrect.
            }
        } else {
            $data ['code'] = "E000100";
            $this->load->view('user_login', $data);
        }
    }

    function logoff() {
        $user_id = $this->session->userdata('userid');
        $array_items = array(
            'userid' => '',
            'user_name' => '',
            'id_number' => '',
            'user_logged' => '',
            'user_type' => ''
        );
        $this->session->unset_userdata($array_items);
        $userdata = array(
            'userid' => '',
            'user_name' => '',
            'id_number' => '',
            'user_logged' => FALSE,
            'user_type' => ''
        );
        $this->session->set_userdata($userdata);
        redirect('user_panel', 'refresh');
    }

    function validateUsername() {
        if (extract($_POST)) {
            $user_name = $_POST ['user_name'];
            $table = "user";
            $where = "where user_name='" . $user_name . "'";
            $row_user = $this->common->getOneRow($table, $where);
            if (isset($row_user) && isset($row_user ['name'])) {
                $data["code"] = "I000000";
                $data["user_name"] = $user_name;
                $data["password"] = $this->encrypt->decode($row_user['password']);
                $this->load->view('change_password', $data);
            } else {
                $data["code"] = "E000100";
                $this->load->view('user_login', $data);
            }
            //echo json_encode($data);
        }
    }

    function changePassword() {
        if (extract($_POST)) {
            $user_name = $_POST ['user_name'];
            $old_password = $_POST ['old_password'];
            $new_password = $_POST ['new_password'];
            $data = self::checkUserValidity($user_name, $old_password);
            if ($data["code"] == "I000000") {
                $value["password"] = $this->encrypt->encode($new_password);
                $value["first_login"] = 'N';
                $where = "user_name = '" . $user_name . "'";
                $this->common->updateRecord("user", $value, $where);
                $user_id = $data ['id'];
                $userdata = array(
                    'userid' => $data ['id'],
                    'user_name' => $data ['user_name'],
                    'id_number' => $data['id_number'],
                    'user_logged' => TRUE
                );
                $this->session->set_userdata($userdata);
                redirect('user_panel', 'refresh');
            } else {
                redirect('user_login/validateUsername/');
            }
        }
    }

    private function checkUserValidity($username, $password) {
        $table = "user";
        $where = "where user_name='" . $username . "' and deleted = 'N' ";
        $row_user = $this->common->getOneRow($table, $where);
        if (isset($row_user) && isset($row_user['password']) && $password == $this->encrypt->decode($row_user['password'])) {
            $data = $row_user;
            $data['password'] = $this->encrypt->decode($row_user['password']);
            $data ['code'] = "I000000";
        } else {
            if (!isset($row_user)) {
                $data ['code'] = "E000100";
            } else {
                $data ['code'] = "E000101";
            }
        }
        return $data;
    }

}
