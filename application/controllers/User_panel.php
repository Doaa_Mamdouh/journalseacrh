<?php
if (! defined ( 'BASEPATH' ))
exit ( 'No direct script access allowed' );

class User_panel extends CI_Controller {
    
	function __construct() {
		parent::__construct ();
		$this->load->model ( "common" );
		$this->load->helper ( array (
		    'form',
		    'url',
		    'file'
		) );
		if ($this->session->userdata ( 'userid' ) == '') {
		    redirect ( 'user_login', 'refresh' );
		}
	}
	
	function index() {
		self::view_advanced_search ();
	}
	
	function view_advanced_search() {
	    $data = array ();
	    if ($this->session->userdata ( 'userid' ) != '') {
	        $user_id = $this->session->userdata ( 'userid' );
	        $where = "where id =" . $user_id;
	        $result = $this->common->getOneRow ( 'user', $where );
	        $data ['login_user_name'] = $result ['name'];
	    }
		$this->load->view ( 'advanced_search', $data );
	}


}
