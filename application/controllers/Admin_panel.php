<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin_panel extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("common");
        $this->load->model("journal_model");
        $this->load->model("user_model");
        $this->load->model("comments_model");
        $this->load->library('encrypt');
        $this->load->helper(array(
            'form',
            'url',
            'file'
        ));
        if ($this->session->userdata('adminid') == '') {
            redirect('admin', 'refresh');
        }
    }

    function index() {
        self::getUsersListView();
    }
    function getUsersListView($cancel = ""){
        $data["users"] = "1";
        if ($cancel == 1) {
            $this->session->set_flashdata ( 'action', "cancel" );
            $this->load->view('general_admin/view_users', $data);
        } else {
            $this->load->view('general_admin/view_users', $data);
        }
    }
    function getUsersList($page_size = 8, $page_number = 1, $screen_message = '', $search_text = '') {
        $users = $this->user_model->getUsers($page_size, $page_number, $search_text);
        $count = $this->user_model->countUsers($search_text);
        if (gettype($users) == "string" || strpos($count, "E") !== false) {
            $data["code"] = "E000100"; //DB Error
            echo json_encode($data);
            return;
        } else {
            $data["users"] = $users;
            $data["total_count"] = $count;
            $data["code"] = "I000000";
            if ($screen_message == 0) {
                $screen_message = "";
            }
            $data["screen_message"] = $screen_message;
            echo json_encode($data);
          
        }
    }

    function updateUser($id) {
        if (extract($_POST)) {
            // apply update action
            $user = $_POST;
            $user["id"] = $id;
            $code = $this->user_model->checkUserRepeat($user["user_name"], $id);
            $code2 = $this->user_model->checkUserRepeatUniversityId($user["id_number"], $id);
            if (gettype($code) == "string") {
                $data = $user;
                $data["code"] = $code; //Repeated
                //echo json_encode($data);
                $this->load->view('general_admin/add_edit_user', $data);
                return;
            } else if (gettype($code2) == "string") {
                $data = $user;
                $data["code"] = $code2; //Repeated ID
                //echo json_encode($data);
                $this->load->view('general_admin/add_edit_user', $data);
                return;
            } else {
                $user['password'] = $this->encrypt->encode($user['password']);
                $this->user_model->updateUser($user);
                //$data["action"] = "update";
                //$this->load->view('general_admin/view_users', $data);
                $this->session->set_flashdata ( 'action', "update" );
                redirect('admin_panel/getUsersListView','refresh');
            }
        } else {
            // get user data
            $user = $this->user_model->getUser($id);
            if (gettype($user) == "string") {
                $data["code"] = $user; //DB Error
                echo json_encode($data);
                return;
            } else {
                // password decoding
                $user['password'] = $this->encrypt->decode($user['password']);
                $data = $user;
                $data["code"] = "I000000";
                //echo json_encode($data);
                $this->load->view('general_admin/add_edit_user', $data);
            }
        }
    }

    function deleteUser($id, $page_size = 8, $page_number = 1) {
        $this->user_model->deleteUser($id);
        $this->session->set_flashdata ( 'action', "delete" );
        redirect('admin_panel/getUsersListView','refresh');
    }

    function createUser() {
        if (extract($_POST)) {
            // apply update action
            $user = $_POST;
            $code = $this->user_model->checkUserRepeat($user["user_name"]);
            $code2 = $this->user_model->checkUserRepeatUniversityId($user["id_number"]);
            if (gettype($code) == "string") {
                $data = $user;
                $data["code"] = $code; //Repeated
                //echo json_encode($data);
                $this->load->view('general_admin/add_edit_user', $data);
                return;
            } else if (gettype($code2) == "string") {
                $data = $user;
                $data["code"] = $code2; //Repeated ID
                //echo json_encode($data);
                $this->load->view('general_admin/add_edit_user', $data);
                return;
            } else {
                $user['password'] = $this->encrypt->encode($user['password']);
                $this->user_model->insertUser($user);
                $this->session->set_flashdata ( 'action', "insert" );
                redirect('admin_panel/getUsersListView','refresh');
            }
        } else {
            $this->load->view('general_admin/add_edit_user');
        }
    }

    function getJournalsListView($cancel = "") {
        $data["journals"] = "1";
        if ($cancel == 1) {
            $this->session->set_flashdata ( 'action', "cancel" );
            $this->load->view('general_admin/view_journals', $data);
        } else {
            $this->load->view('general_admin/view_journals', $data);
        }
    }

    function getJournalsList($page_size = 8, $page_number = 1) {
        $journals = $this->journal_model->getJournals("", $page_size, $page_number, true);
        if (gettype($journals) == "string") {
            $data["code"] = $journals; //DB Error
            echo json_encode($data);
            return;
        } else {

            $count = $this->journal_model->countJournals("", true);
            if (strpos($count, "E") !== false) {
                $data["code"] = $count; //DB Error
                echo json_encode($data);
                return;
            } else {
                $data["journals"] = $journals;
                $data["total_count"] = $count;
                $data["code"] = "I000000"; //Successful
            }
            // if ($page_number > 1) {
            echo json_encode($data);
            // } else {
            //    $this->load->view('general_admin/view_journals', $data);
            // }
        }
    }

    function updateJournal($id) {
        if (extract($_POST)) {
            // apply update action
            $journal = $_POST;
            $journal["id"] = $id;
            // check repeation
            $code = $this->journal_model->checkJournalRepeat($journal["title"], $journal["field"], $id);
            if (gettype($code) == "string") {
                $data = $journal;
                $data["code"] = $code; //Repeated
                //echo json_encode($data);
                $this->load->view('general_admin/add_edit_journal', $data);
                return;
            } else {
                $this->journal_model->updatejournal($journal);
                $this->session->set_flashdata ( 'action', "update" );
                redirect('admin_panel/getJournalsListView','refresh');
            }
        } else {
            // get journal data
            $journal = $this->journal_model->getjournal($id);
            if (gettype($journal) == "string") {
                $data["code"] = $journal; //DB Error
                echo json_encode($data);
                return;
            } else {
                $data = $journal;
                $data["code"] = "I000000";
                $search_creiteria = "";
                $code = $this->journal_model->getDomicileList($search_creiteria);
                if (gettype($code) == "string") {
                    $data["code"] = $code; //DB Error
                    echo json_encode($data);
                    return;
                } else {
                    $data["domicile_list"] = $code;
                    //echo json_encode($data);
                    $this->load->view('general_admin/add_edit_journal', $data);
                }
            }
        }
    }

    function createJournal() {
        if (extract($_POST)) {
            // apply create action
            $journal = $_POST;
            $code = $this->journal_model->checkJournalRepeat($journal["title"], $journal["field"]);
            if (gettype($code) == "string") {
                $data = $journal;
                $data["code"] = $code; //Repeated
                //echo json_encode($data);
                $this->load->view('general_admin/add_edit_journal', $data);
                return;
            } else {
                $code = $this->journal_model->insertJournal($journal);
                if (gettype($code) == "string") {
                    $data["code"] = $code; //DB Error
                    echo json_encode($data);
                    return;
                } else {
                    $this->session->set_flashdata ( 'action', "insert" );
                    redirect('admin_panel/getJournalsListView','refresh');
                }
            }
        } else {
            $search_creiteria = "";
            $code = $this->journal_model->getDomicileList($search_creiteria);
            if (gettype($code) == "string") {
                $data["code"] = $code; //DB Error
                echo json_encode($data);
                return;
            } else {
                $data["domicile_list"] = $code;
                $data["last_update_date"] = date("Y-m-d");
                $this->load->view('general_admin/add_edit_journal', $data);
            }
        }
    }

    function deleteJournal($id) {
        $this->journal_model->deleteJournal($id);
        $this->session->set_flashdata ( 'action', "delete" );
        redirect('admin_panel/getJournalsListView','refresh');
    }

    function view_comments() {
        $data = array();
        $this->load->view('general_admin/view_comments', $data);
    }

    function getPendingCommentsList($page_size = 20, $page_num = 1) {
        try {
            $commentsList_to_send = self::getPendingCommentsListFromModel($page_size, $page_num);
            $count = $this->comments_model->countPendingComments();
            if (gettype($commentsList_to_send) == "string") {
                $data["code"] = $commentsList_to_send; //DB Error
                echo json_encode($data);
                return;
            } else {
                $data["pending_comments"] = $commentsList_to_send;
                if (strpos($count, "E") !== false) {
                    $data["code"] = $count; //DB Error
                    echo json_encode($data);
                    return;
                } else {
                    $data["total_count"] = $count;
                    $data["code"] = "I000000"; //Successful
                }
            }
            echo json_encode($data);
        } catch (Exception $e) {
            $data["code"] = "E999999"; //Unhandled Error
            echo json_encode($data);
        }
    }

    private function getPendingCommentsListFromModel($page_size, $page_num) {
        $result = $this->comments_model->getPendingCommentsList($page_size, $page_num);
        if (gettype($result) == "string") {
            return $result;
        } else {
            $commentsList = $result;
        }
        $commentsList_to_send = array();
        $i = 0;
        foreach ($commentsList as $comment) {
            $commentsList_to_send[$i]["comment_id"] = $comment["comment_id"];
            $commentsList_to_send[$i]["journal_title"] = $comment["journal_title"];
            $commentsList_to_send[$i]["user_name"] = $comment["user_name"];
            $commentsList_to_send[$i]["description"] = $comment["description"];
            $commentsList_to_send[$i]["status"] = $comment["status"];
            $commentsList_to_send[$i]["comment_date"] = $comment["comment_date"];
            $i++;
        }
        return $commentsList_to_send;
    }

}
