<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Admin extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this -> load -> model("common");
        $this->load->helper('url');
        $this->load->library('encrypt');
    }
    
    function index()
    {
        $data['msg']="";
        if($this->session->userdata('adminid')!='') {
            redirect('admin_panel','refresh');
        } else {
            $data['user_name']='';
            $this->load->view('admin_login',$data);
        }
    }
    
    function login () {
        if(extract($_POST)) {
            
            $username=$this->common->mysql_safe_string($this->input->post('user_name'));
            
            $password=$this->common->mysql_safe_string($this->input->post('password'));
            
            if($username!="" && $password!="") {
                $table="admin";
                $where="where user_name='".$username."'";
                $row_admin=$this->common->getOneRow($table,$where);
                if(isset($row_admin) && $password == $this->encrypt->decode($row_admin['password'])) {
                    $userdata = array('adminid'=>$row_admin['id'],'admin_name'=>$row_admin['user_name'],'admin_logged'=>TRUE);
                    $this->session->set_userdata($userdata);
                    redirect('admin_panel','refresh');
                } else {
                    $data['wrong']="Username or password is  incorrect.";
                    $this->load->view('admin_login',$data);
                }
                
            } else {
                $data['msg']="Required fields cannot be left blank.";
                $this->load->view('admin_login',$data);//Executes if username is entered incorrect.
            }
            
        } else {
            $data['msg']="Required fields cannot be left blank.";
            $this->load->view('admin_login',$data);
        }
    }
    
    function logoff()
    {
        
        $array_items = array('adminid' => '','admin_user' => '','admin_logged'=>'',/*'super_admin'=>''*/);
        $this->session->unset_userdata($array_items);
        $userdata = array('adminid'=>'','admin_name'=>'','admin_logged'=>FALSE);
        $this->session->set_userdata($userdata);
        redirect('admin','refresh');
    }
    
}

