<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (! defined('BASEPATH'))
    exit('No direct script access allowed');

class User_Favorites_controller extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model("user_Favorites_model");
    }

    public function index()
    {
        createUserFavoriteJournal();
    }
    
    function createUserFavoriteJournal() {
        if (extract($_POST)) {
            $output = $this->user_Favorites_model->insertUserFavoriteJournal($_POST);
            if (gettype($output) == "string") {
                $data["code"] = $output; //DB Error
                echo json_encode($data);
                return;
            } else {
                //$data["user_favorites_id"] = $output;
                $data["code"] = "I000000"; //Successful
                echo json_encode($data);
            }
        }
    }

    function deleteUserFavoriteJournal() {
        if (extract($_POST)) {
            $output = $this->user_Favorites_model->deleteUserFavoriteJournal($_POST);
            if (gettype($output) == "string") {
                $data["code"] = $output; //DB Error
                echo json_encode($data);
                return;
            } else {
                //$data["affected_records"] = $output;
                $data["code"] = "I000000"; //Successful
                echo json_encode($data);
            }
        }
    }
    
    

}

