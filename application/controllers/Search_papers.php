<?php

defined('BASEPATH') OR exit('No direct script access allowed');

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Search_journals extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("common");
        $this->load->model("journal_model");
        $this->load->model("Search_catalog_model");
    }

    public function index() {
        executeBasicSearch();
    }

    function executeBasicSearch($search_criteria, $page_size = "8", $page_number = "1") {
        try {
            $search_criteria = self::checkPunct($search_criteria);
            $search_criteria_edited = self::getReleValuesFromCatalog($search_criteria);
            $titles = array();
            $titles ["title"] = $search_criteria_edited;
            $titles["title2"] = $search_criteria;
            $journals_to_send = self::getSearchResults($titles, $page_size, $page_number);
            $count = $this->journal_model->countJournals($titles);
            /* $journals_to_send2 = array();
              $count2 = 0;
              if ($search_criteria_edited != trim(urldecode($search_criteria))) {
              $journals_to_send2 = self::getSearchResults(trim(urldecode($search_criteria)), $page_size, $page_number);
              $count2 = $this->journal_model->countJournals(trim(urldecode($search_criteria)));
              } */

//$journals_to_send = array_merge($journals_to_send, $journals_to_send2);
            if (gettype($journals_to_send) == "string") {
                $data["code"] = "E000100"; //DB Error
                echo json_encode($data);
                return;
            } else {
                $data["journals"] = $journals_to_send;
                if (strpos($count, "E") !== false) {
                    $data["code"] = "E000100"; //DB Error
                    echo json_encode($data);
                    return;
                } else {
                    $data["total_count"] = $count + $count2;
                    $data["code"] = "I000000"; //Successful
                }
            }
            echo json_encode($data);
        } catch (Exception $e) {
            $data["code"] = "E999999"; //Unhandled Error
            echo json_encode($data);
        }
    }

    function executeAdvancedSearch($page_size = "8", $page_number = 0) {//$title = "", $field = "", $abs_rank = "", $domicile_ids = "", $articles_number_from = "", $articles_number_to = "", $page_size = "8", $page_number = 0){
        try {
            if (extract($_POST)) {
                $search_criteria = $_POST;
                if ($search_criteria["title"] != "null") {
                    $search_criteria["title"] = self::checkPunct($search_criteria["title"]);
                    $search_criteria["title2"] = $search_criteria["title"];
                    $search_criteria["title"] = self::getReleValuesFromCatalog($search_criteria["title"]);
                } else {
                    $search_criteria["title"] = "null";
                    //$title = "null";
                }
//$search_criteria = self::convertAdvancedParamsToArray($title, $field, $abs_rank, $domicile_ids, $articles_number_from, $articles_number_to);
                $journals_to_send = self::getSearchResults($search_criteria, $page_size, $page_number);
                $count = $this->journal_model->countJournals($search_criteria);
                /* $count2 = 0;
                  if ($title != $search_criteria["title"]) {
                  $search_criteria["title"] = $title;
                  $count2 = $this->journal_model->countJournals($search_criteria);
                  }
                  $search_criteria["title"] = $title;
                  $journals_size = count($journals_to_send);
                  $journals_to_send2 = array();
                  if ($journals_size < $page_size) {
                  $journals_to_send2 = self::getSearchResults($search_criteria, $page_size - $journals_size, $page_number);
                  } */
                if (gettype($journals_to_send) == "string") {
                    $data["code"] = "E000100"; //DB Error
                    echo json_encode($data);
                    return;
                } else {
                    $data["journals"] = $journals_to_send;
                    if (strpos($count, "E") !== false) {
                        $data["code"] = "E000100"; //DB Error F
                        echo json_encode($data);
                        return;
                    } else {
                        $data["total_count"] = $count;
                        $data["code"] = "I000000"; //Successful
                    }
                }

                echo json_encode($data);
            }
        } catch (Exception $e) {
            $data["code"] = "E999999"; //Unhandled Error
            echo json_encode($data);
        }
    }

    function getUserFavoritesJournals($page_size = "8", $page_number = 0, $user_id = null) {
        try {
            if ($user_id == null) {
                $user_id = $this->session->userdata('userid');
            }
            $search_criteria ['user_id'] = $user_id;
            $journals_to_send = self::getSearchResults($search_criteria, $page_size, $page_number, true, true);
            $count = $this->journal_model->countJournals($search_criteria, true, true);
            if (gettype($journals_to_send) == "string") {
                $data["code"] = "E000100"; //DB Error
                echo json_encode($data);
                return;
            } else {
                $data["journals"] = $journals_to_send;
                if (strpos($count, "E") !== false) {
                    $data["code"] = "E000100"; //DB Error F
                    echo json_encode($data);
                    return;
                } else {
                    $data["total_count"] = $count;
                    $data["code"] = "I000000"; //Successful
                }
            }
            echo json_encode($data);
        } catch (Exception $e) {
            $data["code"] = "E999999"; //Unhandled Error
            echo json_encode($data);
        }
    }

    function getAdvancedSearchResultsCount(/* $title = "", $field = "", $abs_rank = "", $domicile_ids = "", $articles_number_from = "", $articles_number_to = "" */) {
        try {
//$search_criteria = self::convertAdvancedParamsToArray($title, $field, $abs_rank, $domicile_ids, $articles_number_from, $articles_number_to);
            if ($_POST) {
                $search_criteria = $_POST;
                if ($search_criteria["title"] != "null") {
                    $search_criteria["title"] = self::checkPunct($search_criteria["title"]);
                    $search_criteria["title2"] = $search_criteria["title"];
                    $search_criteria["title"] = self::getReleValuesFromCatalog($search_criteria["title"]);
                } else {
                    $search_criteria["title"] = "null";
                    $title = "null";
                }
                $count = $this->journal_model->countJournals($search_criteria);
                $domicile_list = $this->journal_model->getDomicileList($search_criteria);
                $fields_list = $this->journal_model->getFieldsList($search_criteria);
                $absRank_list = $this->journal_model->getABSRankList($search_criteria);
                $articleNumBoundaries = $this->journal_model->getArticlesNumberBoundaries($search_criteria);
                /* $count2 = 0;
                  $domicile_list2 = array();
                  $fields_list2 = array();
                  $absRank_list2 = array();
                  $articleNumBoundaries2 = $articleNumBoundaries;
                  if ($title != $search_criteria["title"]) {
                  $search_criteria["title"] = $title;
                  $domicile_list2 = $this->journal_model->getDomicileList($search_criteria);
                  $fields_list2 = $this->journal_model->getFieldsList($search_criteria);
                  $absRank_list2 = $this->journal_model->getABSRankList($search_criteria);
                  $articleNumBoundaries2 = $this->journal_model->getArticlesNumberBoundaries($search_criteria);
                  $count2 = $this->journal_model->countJournals($search_criteria);
                  } */
                $check_fault = gettype($domicile_list) == "string" || gettype($fields_list) == "string" || gettype($absRank_list) == "string" || gettype($articleNumBoundaries) == "string";
                $data = array();
                if (strpos($count, "E") !== false || $check_fault) {
                    $data["code"] = "E000100"; //DB Error
                } else {
                    $data["total_count"] = $count;
                    $list_count = 0;
                    foreach ($domicile_list as $domicile) {
                        $data["domicile_list"][$domicile["id"]]["name"] = $domicile["name"];
                        $list_count = $list_count + 1;
                    }
                    $data["domicile_count"] = $list_count;
                    $list_count = 0;
                    foreach ($fields_list as $field) {
                        $data["fields_list"][$field["name"]]["name"] = $field["name"];
                        $list_count = $list_count + 1;
                    }
                    $data["field_count"] = $list_count;
                    $list_count = 0;
                    foreach ($absRank_list as $rank) {
                        $data["absRank_list"][$rank["name"]]["name"] = $rank["name"];
                        $list_count = $list_count + 1;
                    }
                    $data["absRank_count"] = $list_count;
                    $data["min_articles_number"] = $articleNumBoundaries[0]["min_articles_number"];
                    $data["max_articles_number"] = $articleNumBoundaries[0]["max_articles_number"];
                    $data["code"] = "I000000";
                }
                echo json_encode($data);
            }
        } catch (Exception $e) {
            $data["code"] = "E999999"; //Unhandled Error
            echo json_encode($data);
        }
    }

    function getJournalDetail($journal_id) {
        try {
            $user_id = $this->session->userdata('userid');
            $journal = $this->journal_model->getJournal($journal_id, $user_id);
            if (gettype($journal) == "string") {
                $data["code"] = $journal; //DB Error
            } else {
                $data["journal_detail"] = $journal;
                $data["code"] = "I000000";
            }
            echo json_encode($data);
        } catch (Exception $e) {
            $data["code"] = "E999999"; //Unhandled Error
            echo json_encode($data);
        }
    }

// helpful functions
    private function getReleValuesFromCatalog($search_criteria) {
        $search_criteria = urldecode($search_criteria);
        $search_criteria = trim($search_criteria);
        $search_catalog = $this->Search_catalog_model->getSearchCatalog();
        $matches = array();
        foreach ($search_catalog as $item_key => $value) {
            $search_criteria = strtoupper($search_criteria);
            $key = strtoupper($item_key);
            $lastPos = 0;
            $positions = array();

            while (($lastPos = strpos($search_criteria, $key, $lastPos)) !== false) {
                $positions[] = $lastPos;
                $lastPos = $lastPos + strlen($key);
            }
            //$pos = strpos($search_criteria, $key);
            foreach ($positions as $pos) {
                if ($pos !== false) {
                    $pos_after = false;
                    $pos_before = false;
                    if (strlen($search_criteria) > strlen($key)) {
                        if ($pos > 0) {
                            $pre_pos = $pos - 1;
                            $char_before = substr($search_criteria, $pre_pos, 1);
                            $pos_before = strpos("ABCDEFGHIJKLMNOPQRSTUVXYZ", $char_before);
                        }
                        if ($pos + strlen($key) < strlen($search_criteria)) {
                            $nxt_pos = $pos + strlen($key);
                            $char_after = substr($search_criteria, $nxt_pos, 1);
                            $pos_after = strpos("ABCDEFGHIJKLMNOPQRSTUVXYZ", $char_after);
                        }
                    }
                    if ($pos_before === false && $pos_after === false) {
                        $pos_found = false;
                        foreach ($matches as $match_pos => $match_key) {
                            // R and D === and
                            if($pos >= $match_pos && $pos <= $match_pos+ strlen($match_key)){
                                $pos_found = true;
                                break;
                            }
                            // and === R and D
                            else if ($match_pos >= $pos && $match_pos <= $pos+ strlen($key)){
                                unset($matches[$match_pos]);
                                break;
                            }
                        }
                        if (!$pos_found && !isset($matches[$pos]) ||
                                (isset($matches[$pos]) && strlen($key) > strlen($matches[$pos]))) {
                            $matches[$pos] = $key;
                            //echo $pos ."==". $key." ";
                        }
                    }
                }
            }
        }
        foreach ($matches as $pos => $key) {
           
           $search_criteria = substr_replace($search_criteria,$search_catalog[$key],$pos, strlen($key));
        }
        return $search_criteria;
    }

    function checkPunct($title) {
        $title = urldecode(trim($title));
        $puncts = $this->Search_catalog_model->getPunctuations();
        foreach ($puncts as $item) {
            $title = str_replace($item["key"], $item["value"], $title);
            
        }
        return strtoupper($title);
    }

    private function getSearchResults($search_criteria, $page_size, $page_number, $all_data = false, $fav_only = false) {
        $search_criteria ['user_id'] = $this->session->userdata('userid');
        $result = $this->journal_model->getJournals($search_criteria, $page_size, $page_number, $all_data, $fav_only);
        if (gettype($result) == "string") {
            return $result;
        } else {
            $journals = $result;
        }
        $journals_to_send = array();
        foreach ($journals as $journal) {
            $journals_to_send["journal_id" . $journal["id"]]["id"] = $journal["id"];
            $journals_to_send["journal_id" . $journal["id"]]["title"] = $journal["title"];
            $journals_to_send["journal_id" . $journal["id"]]["field"] = $journal["field"];
            $journals_to_send["journal_id" . $journal["id"]]["abs_rank"] = $journal["abs_rank"];
            $journals_to_send["journal_id" . $journal["id"]]["domicile_name"] = $journal["domicile_name"];
            $journals_to_send["journal_id" . $journal["id"]]["impact_factor"] = $journal["impact_factor"];
            $journals_to_send["journal_id" . $journal["id"]]["frequency"] = $journal["frequency"];
            $journals_to_send["journal_id" . $journal["id"]]["articles_num"] = $journal["articles_num"];
            $journals_to_send["journal_id" . $journal["id"]]["user_favorites_id"] = $journal["user_favorites_id"];
        }
        return $journals_to_send;
    }

    private function convertAdvancedParamsToArray($title, $field, $abs_rank, $domicile_ids, $articles_number_from, $articles_number_to) {

        $search_criteria = array();
        if ($title != "null") {
            $search_criteria["title"] = self::getReleValuesFromCatalog($title);
        } else {
            $search_criteria["title"] = "null";
        }
        if ($field != "null") {
            $search_criteria["field"] = urldecode($field);
        } else {
            $search_criteria["field"] = "null";
        }
        $search_criteria["abs_rank"] = urldecode($abs_rank);
        $search_criteria["domicile_ids"] = urldecode($domicile_ids);
        $search_criteria["articles_number_from"] = $articles_number_from;
        $search_criteria["articles_number_to"] = $articles_number_to;
        return $search_criteria;
    }

}
